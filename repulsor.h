#ifndef REPULSOR_H
#define REPULSOR_H

#include <vector>
#include "vec.h"

#include "pigment.h"

using namespace std;

class Repulsor : public Pigment {
public:
    float c;
    float s;

    Repulsor(Vec2f _position, double _radius);
    ~Repulsor();
    float distance(Vec2f p);
};

#endif // REPULSOR_H
