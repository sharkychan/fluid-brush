#include "fluidvectorfield.h"

FluidVectorField::FluidVectorField(int width, int height) {
    window_width = width;
    window_height = height;

    //value that interpolates between 2 textures
    interpVal = 1.0;

    persistence = 0.8;
    octaves = 2;

    delta_x = 0.0001;

    //Create perlin noise textures
    fastpn = new FastPerlinNoise();
}

//Interpolate the noise produced by two maps
float FluidVectorField::interpNoise(float x, float y) {
    float noise1 = fastpn->noise1(x, y)*interpVal;
    float noise2 = fastpn->noise2(x, y)*(1.0 - interpVal);

    return noise1+noise2;
}

//Taken from Bridson
float FluidVectorField::potential(float x, float y, const Touches& touch) {
    float p = touch.noise_gain;
    if (p==0) return 0;

    //this division adds in curl
    float noiseModifier = interpNoise(x/touch.noise_length, y/touch.noise_length);
    p *= noiseModifier;

    return p;
}

//Taken from Bridson
//Calculate velocity based on x/y positions of particles
Vec2f FluidVectorField::get_velocity(const Vec2f& x, const Touches& touch, float amplifier) {
    Vec2f v = Vec<2, float> (0.0, 0.0);

    float vx = -(potential(x[0], x[1]+delta_x, touch) - potential(x[0], x[1]-delta_x, touch))/(2*delta_x);
    float vy = (potential(x[0]+delta_x, x[1], touch) - potential(x[0]-delta_x, x[1], touch))/(2*delta_x);

    v[0] = vx*amplifier;
    v[1] = vy*amplifier;

    return v;
}

//Increment the interpVal
void FluidVectorField::incrementInterp() {
    if (interpVal > 0) {
        //interpVal is decreasing
        interpVal -= 0.1;
    } else {
        //interpVal is at 0, so switch permuts, shuffle permut2, reset interpVal to 1.0
        fastpn->switchPermuts();
        fastpn->shufflePermut2();
        interpVal = 1.0;
    }
}
