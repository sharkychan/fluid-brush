#ifndef VELOCITYSYSTEM_H
#define VELOCITYSYSTEM_H

#include <QTime>
#include <time.h>

#include <vector>
#include <math.h>
#include <fstream>
#include <string>

#include "vec.h"
#include "util.h"

#include "brushvectorfield.h"
#include "fluidvectorfield.h"

#include "Touches.h"
#include "Stroke.h"
#include "Particle.h"

class VelocitySystem {
public:
    VelocitySystem(int width, int height);
    ~VelocitySystem();

    void printState();

    //Methods for particles
    void generateParticles(Stroke &s, Touches &t0, Touches &t1, float initial_alpha, QGLShaderProgram &_shader);
    void generateParticles(Stroke &s, Touches &t, float initial_alpha, QGLShaderProgram &_shader);
    void moveParticles();
    void resetParticles();

    Touches * handleMidParticles(const Stroke &s, int touch_index, Particle &p);

    //TODO: how much is this vector iterator costing?
    Touches * moveForwardAlongTouches(const Stroke &s, std::vector<Touches *>::const_iterator touch, Particle &p);
    Touches * moveBackwardAlongTouches(const Stroke &s, std::vector<Touches *>::const_reverse_iterator touch, Particle &p);

    float repositionParticle(const Touches &t0, const Touches &t1, Particle &p);
    Touches * reparentParticle(const Stroke &s, Particle &p);
    float distanceFromLineSegment(const Vec2f &t0, const Vec2f &t1, Particle &p);

    //Methods for manipulating touches and particles
    void eraseTouches(int x, int y);
    void eraseParticles(int x, int y);

    //Helper methods
    float clamp(float value);
    void updateParticle(Particle &p, float amplifier);

    void incrementInterp();

    //Methods for storing away particle states
    void createParticleStates();
    void storeCurrentState();
    void adjustCurrentState();

    int window_width;
    int window_height;

    int gridSizeX;
    int gridSizeY;
    int gridCellsX;
    int gridCellsY;

    float simulation_timestep;

    //Connecting velocity systems
    FluidVectorField * fvf;
    BrushVectorField * bvf;

    //Current controllable parameters
    float min_radius;
    float max_radius;
    float mouse_radius;

    float noise_gain;
    float noise_lengthscale;

    float max_density;
    float min_density;

    float initial_alpha;

    Stroke *currentTouchStroke;
    std::vector<Stroke *> strokes;
    std::vector<Particle *> particles;

    QTime *programTime;

    //Storage for capturing particle states each frame
    std::vector< std::vector<Particle *> > particleFrames;
};

#endif //VELOCITYSYSTEM_H
