#include "brushvectorfield.h"

BrushVectorField::BrushVectorField(int _rows, int _columns, int _cell_width, int _cell_height) {
    rows = _rows;
    columns = _columns;
    cell_width = _cell_width;
    cell_height = _cell_height;

    vectorField = new BrushCell[rows*columns];

    initializeVectorField();
    updatedCells.clear();
}

BrushVectorField::~BrushVectorField() {
    delete vectorField;
}

void BrushVectorField::initializeVectorField () {
    for (int i = 0; i < rows*columns; i++) {
        BrushCell cell;
        cell.velocity = Vec<2, float> (0.0, 0.0);
        cell.temp_stroke_id = -1;
        cell.index = i;
        vectorField[i] = cell;
    }
}

//Update the velocity field according to this current brush stroke
//Includes two steps -- inserting initial velocities and diffusing them accordingly
void BrushVectorField::updateVectorField(const Stroke &s) {
    addTouchVelocities(s);
    addStrokeVelocities(s);
    diffuseStrokeVelocities(s);
}

//Add velocities within the cells corresponding to the touch sampling
void BrushVectorField::addTouchVelocities(const Stroke &s) {
    std::vector<Touches *>::const_iterator touch = s.touches.begin();
    for (; touch != s.touches.end(); ++touch) {
        Touches * t0 = (*touch);

        int index = calculateIndex(t0->position);

        if (index >= 0 && index < rows*columns) {
            //Do a check that we won't have an OOB error

            Vec2f velocity = t0->velocity;
            velocity *= t0->directional_amplifier;

            BrushCell * cell = &vectorField[index];

            //Add stroke id to cell
            cell->velocity += velocity;
            if (!containsStrokeId(cell->stroke_ids, s.id)) {
                cell->stroke_ids.push_back(s.id);
                cell->temp_stroke_id = s.id;
            }
        }
    }
}

//Add velocities along stroke's center line
//These should not have any gaps and should diffuse according to touch velocities
void BrushVectorField::addStrokeVelocities(const Stroke &s) {
    std::vector<Touches *>::const_iterator touch = s.touches.begin();
    Touches * t0 = (*touch);
    ++touch;
    for (; touch != s.touches.end(); ++touch) {
        Touches * t1 = (* touch);

        float distance = dist(t0->position, t1->position);

        if (distance != 0.0) {
            float increment = 1.0/min(cell_width, cell_height);
            //Find all cells along the line segment between p0 and p1
            for (float d = 0.0; d <= 1.0; d += increment) {
                Vec2f position = lerp(t0->position, t1->position, d);
                int index = calculateIndex(position);

                if (index >= 0 && index < rows*columns) {
                    //Do a check that we won't have an OOB error

                    Vec2f velocity = lerp(t0->velocity, t1->velocity, d);
                    velocity *= t0->directional_amplifier;

                    BrushCell * cell = &vectorField[index];

                    //Add stroke id to cell
                    cell->velocity = velocity;
                    if (!containsStrokeId(cell->stroke_ids, s.id)) {
                        cell->stroke_ids.push_back(s.id);
                        cell->temp_stroke_id = s.id;
                    }
                }
            }
        }

        t0 = t1;
    }
}

//TODO: combine this with do cubic interp using lambdas
void BrushVectorField::doCosineInterp(const Touches &t0, const Touches &t1, int id) {
    float distance = dist(t0.position, t1.position);

    //This x2 hack ensures all cells between points are touched...
    float increment = distance/min(cell_width*2.0, cell_height*2.0);

    if (distance != 0.0) {
        float norm_increment = increment/distance;
        //Find all cells along the line segment between p0 and p1
        for (float d = 0.0; d <= 1.0; d += norm_increment) {
            Vec2f position = cosineInterpolation(t0.position, t1.position, d);
            int index = calculateIndex(position);

            if (index >= 0 && index < rows*columns) {
                //Do a check that we won't have an OOB error

                Vec2f velocity = cosineInterpolation(t0.velocity, t1.velocity, d);
                velocity *= t0.directional_amplifier;

                BrushCell * cell = &vectorField[index];

                //Add stroke id to cell
                cell->velocity = velocity;
                if (!containsStrokeId(cell->stroke_ids, id)) {
                    cell->stroke_ids.push_back(id);
                    cell->temp_stroke_id = id;
                }
            }
        }
    }
}

void BrushVectorField::doCubicInterp(const Touches &t0, const Touches &t1, const Touches &t2, const Touches &t3, int id) {
    float distance = dist(t1.position, t2.position);

    //This x2 hack ensures all cells between points are touched...
    float increment = distance/min(cell_width*2.0, cell_height*2.0);

    if (distance != 0.0) {
        float norm_increment = increment/distance;
        //Find all cells along the line segment between p0 and p1
        for (float d = 0.0; d <= 1.0; d += norm_increment) {
            Vec2f position = cubicInterpolation(t0.position, t1.position, t2.position, t3.position, d);
            int index = calculateIndex(position);

            if (index >= 0 && index < rows*columns) {
                //Do a check that we won't have an OOB error
                Vec2f velocity = cubicInterpolation(t0.velocity, t1.velocity, t2.velocity, t3.velocity, d);
                velocity *= t1.directional_amplifier;

                BrushCell * cell = &vectorField[index];

                //Add stroke id to cell
                cell->velocity = velocity;
                if (!containsStrokeId(cell->stroke_ids, id)) {
                    cell->stroke_ids.push_back(id);
                    cell->temp_stroke_id = id;
                }
            }
        }
    }
}

//Diffuse all cells related to the current brushstroke
void BrushVectorField::diffuseStrokeVelocities(const Stroke &s) {
    int count = 0;

    float cellsize = std::min(cell_width, cell_height);
    float maxradius = 0;
    std::vector<Touches *>::const_iterator touch;
    for (touch = s.touches.begin(); touch != s.touches.end(); ++touch) {
        Touches * t = (Touches *)(* touch);
        if (maxradius < t->radius) {
            maxradius = t->radius;
        }
    }

    int total_iterations = (int)ceil(maxradius/cellsize)*2;
    while (count < total_iterations) {

        //Check all cells for current stroke s
        for (int i = 1; i < rows-1; i++) {
            for (int j = 1; j < columns-1; j++) {
                BrushCell * current = &vectorField[i*columns+j];

                if (containsStrokeId(current->stroke_ids, s.id)) {
                    //Flag all neighboring stroke cells with temp_stroke_id

                    BrushCell * east = &vectorField[(i+1)*columns + j];
                    east->temp_stroke_id = s.id;
                    BrushCell * west = &vectorField[(i-1)*columns + j];
                    west->temp_stroke_id = s.id;
                    BrushCell * north = &vectorField[i*columns + (j+1)];
                    north->temp_stroke_id = s.id;
                    BrushCell * south = &vectorField[i*columns + (j-1)];
                    south->temp_stroke_id = s.id;

                    BrushCell * ne = &vectorField[(i+1)*columns + (j+1)];
                    ne->temp_stroke_id = s.id;
                    BrushCell * nw = &vectorField[(i-1)*columns + (j+1)];
                    nw->temp_stroke_id = s.id;
                    BrushCell * se = &vectorField[(i+1)*columns + (j-1)];
                    se->temp_stroke_id = s.id;
                    BrushCell * sw = &vectorField[(i-1)*columns + (j-1)];
                    sw->temp_stroke_id = s.id;
                }
            }
        }

        //Check all cells for temp_stroke_id
        for (int i = 1; i < rows-1; i++) {
            for (int j = 1; j < columns-1; j++) {
                BrushCell * current = &vectorField[i*columns+j];

                if (current->temp_stroke_id == s.id) {
                    //Calculate diffusion for all cells with temp_stroke_id

                    BrushCell east = vectorField[(i+1)*columns + j];
                    BrushCell west = vectorField[(i-1)*columns + j];
                    BrushCell north = vectorField[i*columns + (j+1)];
                    BrushCell south = vectorField[i*columns + (j-1)];

                    BrushCell ne = vectorField[(i+1)*columns + (j+1)];
                    BrushCell nw = vectorField[(i-1)*columns + (j+1)];
                    BrushCell se = vectorField[(i+1)*columns + (j-1)];
                    BrushCell sw = vectorField[(i-1)*columns + (j-1)];

                    Vec2f total = east.velocity + west.velocity + north.velocity + south.velocity +
                            ne.velocity + nw.velocity + se.velocity + sw.velocity;

                    total *= 0.125;

                    BrushCell cell;
                    cell.index = i*columns+j;
                    cell.velocity = total;
                    cell.stroke_ids = vectorField[i*columns+j].stroke_ids;

                    updatedCells.push_back(cell);
                }
            }
        }

        //Move velocities stored in updatedCells back into the vectorField
        std::vector<BrushCell>::iterator cell;
        for (cell = updatedCells.begin(); cell != updatedCells.end(); ++cell) {
            BrushCell bc = (*cell);
            vectorField[bc.index].velocity = bc.velocity;
        }

        //Clear updatedCells
        updatedCells.clear();

        //Add strokes with the temp_stroke_id flag to stroke_id vector
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                BrushCell * current = &vectorField[i*columns+j];

                if (current->temp_stroke_id == s.id && !containsStrokeId(current->stroke_ids, s.id)) {
                    current->stroke_ids.push_back(s.id);
                }
            }
        }

        count++;
    }

}

//Return the velocity vector based on position in the grid
Vec2f BrushVectorField::get_velocity(const Vec2f &position) {
    int particle_index = calculateIndex(position);
    return vectorField[particle_index].velocity;
}

//Return the cell's velocity vector based on its index
Vec2f BrushVectorField::getCellVelocity(int index) {
    return vectorField[index].velocity;
}

Vec2f BrushVectorField::getCellVelocity(const Vec2f &position) {
    int index = calculateIndex(position);
    return getCellVelocity(index);
}


int BrushVectorField::getStrokeId(int index) {
    return vectorField[index].temp_stroke_id;
}

void BrushVectorField::removeStroke(const Stroke &s) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            if (containsStrokeId(vectorField[i*columns+j].stroke_ids, s.id)) {
                //TODO: go through and remove pointer to stroke s at necessary index on erase
            }
        }
    }
}

void BrushVectorField::printState() {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            BrushCell cell = vectorField[i*columns+j];
            printf("(%f, %f) ", cell.velocity[0], cell.velocity[1]);
        }
        printf("\n");
    }
}


//Find the index within the vector field that's associated with a given position
int BrushVectorField::calculateIndex(const Vec2f &position) {
    int xcell = floorf(position[0]/cell_width);
    int ycell = floorf(position[1]/cell_height);
    return xcell*columns+ycell;
}

bool BrushVectorField::containsStrokeId(std::vector<int> cell_strokes, int _id) {
    std::vector<int>::iterator id;
    for (id = cell_strokes.begin(); id != cell_strokes.end(); ++id) {
        if (_id == *id) {
            return true;
        }
    }

    return false;
}

Vec2f BrushVectorField::lerp(const Vec2f &v0, const Vec2f &v1, float t) {
    return v0 + t * (v1 - v0);
}

Vec2f BrushVectorField::cosineInterpolation(const Vec2f &p0, const Vec2f &p1, float mu) {
   float mu2 = (1 - cosf(mu*M_PI))/2;
   return (p0*(1 - mu2) + p1*mu2);
}

//Cubic interpolation between points p1 and p2 at position mu along line
//TODO: combine cubic and cosine interp from this class with stroke class's in separate library (vec?)
Vec2f BrushVectorField::cubicInterpolation(const Vec2f &p0, const Vec2f &p1, const Vec2f &p2, const Vec2f &p3, float mu) {
    Vec2f position = Vec<2, float> (0., 0.);

    float mu2 = mu*mu;

    float a0 = p3[0] - p2[0] - p0[0] + p1[0];
    float a1 = p0[0] - p1[0] - a0;
    float a2 = p2[0] - p0[0];
    float a3 = p1[0];

    position[0] = a0*mu*mu2 + a1*mu2 + a2*mu + a3;

    a0 = p3[1] - p2[1] - p0[1] + p1[1];
    a1 = p0[1] - p1[1] - a0;
    a2 = p2[1] - p0[1];
    a3 = p1[1];

    position[1] = a0*mu*mu2 + a1*mu2 + a2*mu + a3;

    return position;
}
