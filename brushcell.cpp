#include "Brushcell.h"

Brushcell::Brushcell() {
    velocity = Vec<2, float> (0.0, 0.0);
}

Brushcell::~Brushcell() {
}

Vec2f Brushcell::getVelocity() {
    return velocity;
}

void Brushcell::setVelocity(Vec2f _velocity) {
    velocity = _velocity;
}


void Brushcell::addStroke(Stroke *s) {
    cellStrokes.push_back(s);
}
