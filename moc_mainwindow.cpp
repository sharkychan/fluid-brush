/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[32];
    char stringdata[557];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 22),
QT_MOC_LITERAL(2, 34, 0),
QT_MOC_LITERAL(3, 35, 23),
QT_MOC_LITERAL(4, 59, 23),
QT_MOC_LITERAL(5, 83, 23),
QT_MOC_LITERAL(6, 107, 34),
QT_MOC_LITERAL(7, 142, 34),
QT_MOC_LITERAL(8, 177, 32),
QT_MOC_LITERAL(9, 210, 12),
QT_MOC_LITERAL(10, 223, 10),
QT_MOC_LITERAL(11, 234, 13),
QT_MOC_LITERAL(12, 248, 11),
QT_MOC_LITERAL(13, 260, 11),
QT_MOC_LITERAL(14, 272, 12),
QT_MOC_LITERAL(15, 285, 10),
QT_MOC_LITERAL(16, 296, 10),
QT_MOC_LITERAL(17, 307, 16),
QT_MOC_LITERAL(18, 324, 15),
QT_MOC_LITERAL(19, 340, 16),
QT_MOC_LITERAL(20, 357, 17),
QT_MOC_LITERAL(21, 375, 7),
QT_MOC_LITERAL(22, 383, 30),
QT_MOC_LITERAL(23, 414, 21),
QT_MOC_LITERAL(24, 436, 31),
QT_MOC_LITERAL(25, 468, 22),
QT_MOC_LITERAL(26, 491, 14),
QT_MOC_LITERAL(27, 506, 4),
QT_MOC_LITERAL(28, 511, 16),
QT_MOC_LITERAL(29, 528, 6),
QT_MOC_LITERAL(30, 535, 15),
QT_MOC_LITERAL(31, 551, 5)
    },
    "MainWindow\0on_actionNew_triggered\0\0"
    "on_actionOpen_triggered\0on_actionSave_triggered\0"
    "on_actionQuit_triggered\0"
    "on_actionReset_Particles_triggered\0"
    "on_actionFragment_Shader_triggered\0"
    "on_actionVertex_Shader_triggered\0"
    "togglePoints\0showPoints\0toggleTexture\0"
    "showTexture\0toggleSmoke\0toggleBubble\0"
    "toggleFire\0toggleStar\0on_reset_clicked\0"
    "on_quit_clicked\0on_stats_clicked\0"
    "on_radius_updated\0_radius\0"
    "on_turbulenceAmplifier_updated\0"
    "_turbulence_amplifier\0"
    "on_directionalAmplifier_updated\0"
    "_directional_amplifier\0on_red_updated\0"
    "_red\0on_green_updated\0_green\0"
    "on_blue_updated\0_blue"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x08 /* Private */,
       3,    0,  125,    2, 0x08 /* Private */,
       4,    0,  126,    2, 0x08 /* Private */,
       5,    0,  127,    2, 0x08 /* Private */,
       6,    0,  128,    2, 0x08 /* Private */,
       7,    0,  129,    2, 0x08 /* Private */,
       8,    0,  130,    2, 0x08 /* Private */,
       9,    1,  131,    2, 0x08 /* Private */,
      11,    1,  134,    2, 0x08 /* Private */,
      13,    0,  137,    2, 0x08 /* Private */,
      14,    0,  138,    2, 0x08 /* Private */,
      15,    0,  139,    2, 0x08 /* Private */,
      16,    0,  140,    2, 0x08 /* Private */,
      17,    0,  141,    2, 0x08 /* Private */,
      18,    0,  142,    2, 0x08 /* Private */,
      19,    0,  143,    2, 0x08 /* Private */,
      20,    1,  144,    2, 0x08 /* Private */,
      22,    1,  147,    2, 0x08 /* Private */,
      24,    1,  150,    2, 0x08 /* Private */,
      26,    1,  153,    2, 0x08 /* Private */,
      28,    1,  156,    2, 0x08 /* Private */,
      30,    1,  159,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void, QMetaType::Int,   23,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, QMetaType::Int,   29,
    QMetaType::Void, QMetaType::Int,   31,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_actionNew_triggered(); break;
        case 1: _t->on_actionOpen_triggered(); break;
        case 2: _t->on_actionSave_triggered(); break;
        case 3: _t->on_actionQuit_triggered(); break;
        case 4: _t->on_actionReset_Particles_triggered(); break;
        case 5: _t->on_actionFragment_Shader_triggered(); break;
        case 6: _t->on_actionVertex_Shader_triggered(); break;
        case 7: _t->togglePoints((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->toggleTexture((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->toggleSmoke(); break;
        case 10: _t->toggleBubble(); break;
        case 11: _t->toggleFire(); break;
        case 12: _t->toggleStar(); break;
        case 13: _t->on_reset_clicked(); break;
        case 14: _t->on_quit_clicked(); break;
        case 15: _t->on_stats_clicked(); break;
        case 16: _t->on_radius_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->on_turbulenceAmplifier_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_directionalAmplifier_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->on_red_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->on_green_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->on_blue_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
