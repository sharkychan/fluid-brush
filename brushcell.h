#ifndef BRUSHCELL_H
#define BRUSHCELL_H

#include "vec.h"
#include "Stroke.h"
#include <list>

//TODO: make this a struct rather than a class _o_
class Brushcell {
public:
    Vec2f velocity;
    list<Stroke *> cellStrokes;

    Brushcell();
    ~Brushcell();

    Vec2f getVelocity();
    void setVelocity(Vec2f _velocity);
    void addStroke(Stroke *s);

};

#endif // BRUSHCELL_H
