#include <QGuiApplication>
#include "mainwindow.h"

#include "tabletapplication.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]) {
    Q_INIT_RESOURCE(textures);

    //Seed random
    srand(time(nullptr));

    setbuf(stdout, nullptr); //Allow program to print immediately to console

    TabletApplication app(argc, argv);
    MainWindow w;
    w.show();
    return app.exec();
}
