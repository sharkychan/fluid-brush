/*
 *  Stroke.cpp
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 11/28/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "Stroke.h"
#include <stdio.h>

#include "vec.h"
#include "util.h"

Stroke::Stroke(float _initial_alpha, int _id) {
    initial_alpha = _initial_alpha;
    id = _id;
}

Stroke::~Stroke() {
    touches.clear();
}

//TODO: verify touch removal doesn't break the system
void Stroke::eraseTouches(int x, int y, float radius) {
    std::vector<Touches *>::iterator touch;
    for (touch = touches.begin(); touch != touches.end();) {
        //Look through all touches within a stroke
        Touches * t = (*touch);
        Vec2f position = Vec<2, float> (x, y);
        float distance = dist(t->position, position);
        if (distance <= radius) {
            //Remove touch from stroke
            touch = touches.erase(touch);
        } else {
            ++touch;
        }
    }
}
