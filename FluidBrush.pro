# -------------------------------------------------
# Project created by QtCreator 2013-08-02T11:18:15
# -------------------------------------------------
QT += opengl \
    widgets
TARGET = FluidBrush
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    FastPerlinNoise.cpp \
    Touches.cpp \
    Stroke.cpp \
    Particle.cpp \
    canvaswidget.cpp \
    tabletapplication.cpp \
    brushvectorfield.cpp \
    fluidvectorfield.cpp \
    velocitysystem.cpp
HEADERS += mainwindow.h \
    util.h \
    vec.h \
    FastPerlinNoise.h \
    Touches.h \
    Stroke.h \
    Particle.h \
    canvaswidget.h \
    Point.h \
    tabletapplication.h \
    brushvectorfield.h \
    fluidvectorfield.h \
    velocitysystem.h
FORMS += mainwindow.ui
OTHER_FILES += 
RESOURCES += textures.qrc \
    shaders.qrc
QMAKE_MAC_SDK = macosx10.9
QMAKE_CXXFLAGS += -std=c++11
