/*
 *  FastPerlinNoise.cpp
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 12/3/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "FastPerlinNoise.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include <stdio.h>

FastPerlinNoise::FastPerlinNoise() {
        srand(time(nullptr));

        half_n = 256;
        n = half_n*2;
        permut = new int[n];
        permut1 = new int[n];
        permut2 = new int[n];

        int permutation[] = { 151,160,137,91,90,15,
                131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
                190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
                88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
                77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
                102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
                135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
                5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
                223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
                129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
                251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
                49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
                138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
        };

        for (int i = 0; i < half_n; i++) {
                permut[i] = permut[half_n+i] = permutation[i];
                permut1[i] = permut1[half_n+i] = permutation[i];
                permut2[i] = permut2[half_n+i] = permutation[i];
        }
}

FastPerlinNoise::~FastPerlinNoise() {
        delete permut;
        delete permut1;
        delete permut2;
}

//Randomly reshuffle the values in permut
void FastPerlinNoise::shufflePermut() {
        for (int i = n-1; i >= 1; i--) {
                int j = rand() % (i+1);
                int temp = permut[i];
                permut[i] = permut[j];
                permut[j] = temp;
        }
}

//Randomly reshuffle the values in permut1
void FastPerlinNoise::shufflePermut1() {
        for (int i = n-1; i >= 1; i--) {
                int j = rand() % (i+1);
                int temp = permut1[i];
                permut1[i] = permut1[j];
                permut1[j] = temp;
        }
}

//Randomly reshuffle the values in permut2
void FastPerlinNoise::shufflePermut2() {
        for (int i = n-1; i >= 1; i--) {
                int j = rand() % (i+1);
                int temp = permut2[i];
                permut2[i] = permut2[j];
                permut2[j] = temp;
        }
}

//Switch the pointers to the two permuts
void FastPerlinNoise::switchPermuts() {
        int *temp = permut1;
        permut1 = permut2;
        permut2 = temp;
}

float FastPerlinNoise::fade(float t) {
        //return t * t * t * (t * (t * 6 - 15) + 10);
        return 3*t*t - 2*t*t*t;
}

float FastPerlinNoise::grad(int hash, float x, float y) {
        //We only need 4 gradient directions, so only need for 2 bits
        int h = hash & 3;
        float u = x;
        float v = y;

        if (h == 1) {
                u = -x;
        } else if (h == 2) {
                v = -y;
        } else if (h == 3) {
                u = -x;
                v = -y;
        }

        return u + v;
}

float FastPerlinNoise::lerp(float t, float a, float b) {
        return a + t * (b - a);
}

float FastPerlinNoise::noise(float x, float y) {
        int X = (int)floor(x) & (half_n-1);
        int Y = (int)floor(y) & (half_n-1);

        x -= floor(x);
        y -= floor(y);

        float ux = fade(x);
        float vy = fade(y);

        int A = permut[X]+Y;
        int AA = permut[A];
        int AB = permut[A+1];

        int B = permut[X+1]+Y;
        int BA = permut[B];
        int BB = permut[B+1];

        float s = grad(permut[AA], x, y);
        float t = grad(permut[BA], x-1, y);
        float u = grad(permut[AB], x, y-1);
        float v = grad(permut[BB], x-1, y-1);
        return lerp(vy, lerp(ux, s, t), lerp(ux, u, v));

}

float FastPerlinNoise::noise1(float x, float y) {
        int X = (int)floor(x) & (half_n-1);
        int Y = (int)floor(y) & (half_n-1);

        x -= floor(x);
        y -= floor(y);

        float ux = fade(x);
        float vy = fade(y);

        int A = permut1[X]+Y;
        int AA = permut1[A];
        int AB = permut1[A+1];

        int B = permut1[X+1]+Y;
        int BA = permut1[B];
        int BB = permut1[B+1];

        float s = grad(permut1[AA], x, y);
        float t = grad(permut1[BA], x-1, y);
        float u = grad(permut1[AB], x, y-1);
        float v = grad(permut1[BB], x-1, y-1);
        return lerp(vy, lerp(ux, s, t), lerp(ux, u, v));

}

float FastPerlinNoise::noise2(float x, float y) {
        int X = (int)floor(x) & (half_n-1);
        int Y = (int)floor(y) & (half_n-1);

        x -= floor(x);
        y -= floor(y);

        float ux = fade(x);
        float vy = fade(y);

        int A = permut2[X]+Y;
        int AA = permut2[A];
        int AB = permut2[A+1];

        int B = permut2[X+1]+Y;
        int BA = permut2[B];
        int BB = permut2[B+1];

        float s = grad(permut2[AA], x, y);
        float t = grad(permut2[BA], x-1, y);
        float u = grad(permut2[AB], x, y-1);
        float v = grad(permut2[BB], x-1, y-1);
        return lerp(vy, lerp(ux, s, t), lerp(ux, u, v));

}

float FastPerlinNoise::perlinNoise(float x, float y, float persistence, int octaves) {
        float color = 0.0;
        for (int i = 0; i < octaves; i++) {
                float frequency = pow(2, i);
                float amplitude = pow(persistence, i);
                color += noise(x*frequency, y*frequency)*amplitude;
        }

        return color;
}

float * FastPerlinNoise::createNoiseTexture(int width, int height, float persistence, int octaves) {
        float * perlinTexture = new float[width*height*3];

        for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                        float jitter1 = (float)rand()/((float)RAND_MAX/2);
                        float jitter2 = (float)rand()/((float)RAND_MAX/2);
                        float color = perlinNoise(i+jitter1, j+jitter2, persistence, octaves);
                        perlinTexture[i*height*3+j*3] = color;
                        perlinTexture[i*height*3+j*3+1] = color;
                        perlinTexture[i*height*3+j*3+2] = color;
                }
        }

        return perlinTexture;
}
