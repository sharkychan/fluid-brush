/*
 *  Particle.h
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 3/19/13.
 *  Copyright 2013 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _PARTICLE
#define _PARTICLE

#include <QGLShaderProgram>
#include "Stroke.h"
#include "Touches.h"

#include "vec.h"

class Particle {
public:
    Vec2f position;
    float radius;
    float alpha;
    float distance; //Distance from the parent touch line segment
    Vec2f perpendicular_position;  //Point on line segment perpendicular to the particle

    Stroke * parent_stroke;
    Touches * parent_touch;

    float red;
    float green;
    float blue;

    QGLShaderProgram * shader;

    Vec2f v;        //Velocity
    float w;        //Angular velocity

    Particle(Stroke * s, Touches * t, Vec2f _position, float _distance, float _alpha);
    Particle(Particle * p);
    ~Particle();
}; 
#endif
