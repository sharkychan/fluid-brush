/*
 *  Stroke.h
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 11/28/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _STROKE
#define _STROKE

#include "Touches.h"
#include <vector>

class Stroke {
public:
    int id;
    std::vector<Touches *> points;
    std::vector<Touches *> touches;
    float initial_alpha;

    Stroke(float _initial_alpha, int _id);
    ~Stroke();
    void eraseTouches(int x, int y, float radius);

};
#endif
