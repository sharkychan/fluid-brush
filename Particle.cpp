/*
 *  Particle.cpp
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 3/19/13.
 *  Copyright 2013 __MyCompanyName__. All rights reserved.
 *
 */

#include "Particle.h"

Particle::Particle(Stroke * s, Touches * t, Vec2f _position, float _distance, float _alpha) {
    position = _position;
    distance = _distance;
    perpendicular_position = Vec<2, float> (0.0, 0.0);
    radius = (float)(rand() % 7) + 6;
    alpha = _alpha;
    v = Vec<2, float> (0.0, 0.0);
    w = 0.0;

    parent_stroke = s;
    parent_touch = t;
}

//Deep copy of particle p to new particle
Particle::Particle(Particle * p) {
    position = p->position;
    radius = p->radius;
    alpha = p->alpha;
    distance = p->distance;
    perpendicular_position = p->perpendicular_position;
    v = p->v;
    w = p->w;
    red = p->red;
    green = p->green;
    blue = p->blue;
    shader = p->shader;

    parent_stroke = p->parent_stroke;
    parent_touch = p->parent_touch;
}

Particle::~Particle() {

}
