/*
 *  Touches.h
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 10/30/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _TOUCHES
#define _TOUCHES

#include <QGLShaderProgram>

#include <vector>

#include "vec.h"

class Touches {
public:
    Vec2f position;
    double radius;
    int index;

    Vec2f above;
    Vec2f below;

    //controllable
    float density;
    float turbulence_amplifier;
    float directional_amplifier;

    double noise_gain;
    double noise_length;

    Vec2f velocity;             //Stroke Velocity

    float red;
    float green;
    float blue;

    Touches(Vec2f _position, double _noise_gain, double _noise_length);
    Touches(Touches * _t);
    ~Touches();

    void setTouchParameters(float _density, float _radius, float _turbulence, float _directional);
    void setColor(float _red, float _green, float _blue);
    void setVelocity(Touches * prevTouch, int _culledPoints);
    void setIndex(int _index);

    void generateOuterPoints(Vec2f endpoint);

    void printout();
};
#endif
