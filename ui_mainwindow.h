/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QAction *actionOpen;
    QAction *actionNew;
    QAction *actionSave;
    QAction *actionFragment_Shader;
    QAction *actionVertex_Shader;
    QAction *actionReset_Particles;
    QAction *actionQuit;
    QWidget *centralWidget;
    QSlider *sliderRadius;
    QLabel *MouseRadius;
    QSlider *sliderTurbulenceAmp;
    QLabel *VelocityAmplifer;
    QCheckBox *checkBoxPoints;
    QCheckBox *checkBoxTexture;
    QPushButton *stats;
    QGroupBox *groupBox;
    QSlider *sliderRed;
    QSlider *sliderBlue;
    QSlider *sliderGreen;
    QLabel *labelRed;
    QLabel *labelBlue;
    QLabel *labelGreen;
    QGraphicsView *colorDisplay;
    QSlider *sliderDirectionalAmp;
    QLabel *VelocityAmplifer_2;
    QGroupBox *groupBox_2;
    QRadioButton *radioSmoke;
    QRadioButton *radioBubbles;
    QRadioButton *radioStars;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QStringLiteral("MainWindowClass"));
        MainWindowClass->resize(337, 470);
        actionOpen = new QAction(MainWindowClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionNew = new QAction(MainWindowClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionSave = new QAction(MainWindowClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionFragment_Shader = new QAction(MainWindowClass);
        actionFragment_Shader->setObjectName(QStringLiteral("actionFragment_Shader"));
        actionVertex_Shader = new QAction(MainWindowClass);
        actionVertex_Shader->setObjectName(QStringLiteral("actionVertex_Shader"));
        actionReset_Particles = new QAction(MainWindowClass);
        actionReset_Particles->setObjectName(QStringLiteral("actionReset_Particles"));
        actionQuit = new QAction(MainWindowClass);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        sliderRadius = new QSlider(centralWidget);
        sliderRadius->setObjectName(QStringLiteral("sliderRadius"));
        sliderRadius->setGeometry(QRect(20, 30, 181, 22));
        sliderRadius->setOrientation(Qt::Horizontal);
        MouseRadius = new QLabel(centralWidget);
        MouseRadius->setObjectName(QStringLiteral("MouseRadius"));
        MouseRadius->setGeometry(QRect(20, 10, 111, 17));
        sliderTurbulenceAmp = new QSlider(centralWidget);
        sliderTurbulenceAmp->setObjectName(QStringLiteral("sliderTurbulenceAmp"));
        sliderTurbulenceAmp->setGeometry(QRect(20, 80, 181, 22));
        sliderTurbulenceAmp->setOrientation(Qt::Horizontal);
        VelocityAmplifer = new QLabel(centralWidget);
        VelocityAmplifer->setObjectName(QStringLiteral("VelocityAmplifer"));
        VelocityAmplifer->setGeometry(QRect(20, 60, 141, 17));
        checkBoxPoints = new QCheckBox(centralWidget);
        checkBoxPoints->setObjectName(QStringLiteral("checkBoxPoints"));
        checkBoxPoints->setGeometry(QRect(220, 40, 111, 21));
        checkBoxTexture = new QCheckBox(centralWidget);
        checkBoxTexture->setObjectName(QStringLiteral("checkBoxTexture"));
        checkBoxTexture->setGeometry(QRect(220, 10, 111, 21));
        stats = new QPushButton(centralWidget);
        stats->setObjectName(QStringLiteral("stats"));
        stats->setGeometry(QRect(220, 70, 113, 32));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 160, 191, 151));
        sliderRed = new QSlider(groupBox);
        sliderRed->setObjectName(QStringLiteral("sliderRed"));
        sliderRed->setGeometry(QRect(79, 30, 101, 22));
        sliderRed->setOrientation(Qt::Horizontal);
        sliderBlue = new QSlider(groupBox);
        sliderBlue->setObjectName(QStringLiteral("sliderBlue"));
        sliderBlue->setGeometry(QRect(80, 70, 101, 22));
        sliderBlue->setOrientation(Qt::Horizontal);
        sliderGreen = new QSlider(groupBox);
        sliderGreen->setObjectName(QStringLiteral("sliderGreen"));
        sliderGreen->setGeometry(QRect(80, 110, 101, 22));
        sliderGreen->setOrientation(Qt::Horizontal);
        labelRed = new QLabel(groupBox);
        labelRed->setObjectName(QStringLiteral("labelRed"));
        labelRed->setGeometry(QRect(80, 50, 61, 17));
        labelBlue = new QLabel(groupBox);
        labelBlue->setObjectName(QStringLiteral("labelBlue"));
        labelBlue->setGeometry(QRect(80, 90, 61, 17));
        labelGreen = new QLabel(groupBox);
        labelGreen->setObjectName(QStringLiteral("labelGreen"));
        labelGreen->setGeometry(QRect(80, 130, 61, 17));
        colorDisplay = new QGraphicsView(groupBox);
        colorDisplay->setObjectName(QStringLiteral("colorDisplay"));
        colorDisplay->setGeometry(QRect(10, 30, 51, 101));
        sliderDirectionalAmp = new QSlider(centralWidget);
        sliderDirectionalAmp->setObjectName(QStringLiteral("sliderDirectionalAmp"));
        sliderDirectionalAmp->setGeometry(QRect(20, 130, 181, 22));
        sliderDirectionalAmp->setOrientation(Qt::Horizontal);
        VelocityAmplifer_2 = new QLabel(centralWidget);
        VelocityAmplifer_2->setObjectName(QStringLiteral("VelocityAmplifer_2"));
        VelocityAmplifer_2->setGeometry(QRect(20, 110, 141, 17));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(20, 320, 141, 91));
        radioSmoke = new QRadioButton(groupBox_2);
        radioSmoke->setObjectName(QStringLiteral("radioSmoke"));
        radioSmoke->setGeometry(QRect(10, 20, 102, 20));
        radioBubbles = new QRadioButton(groupBox_2);
        radioBubbles->setObjectName(QStringLiteral("radioBubbles"));
        radioBubbles->setGeometry(QRect(10, 40, 102, 20));
        radioStars = new QRadioButton(groupBox_2);
        radioStars->setObjectName(QStringLiteral("radioStars"));
        radioStars->setGeometry(QRect(10, 60, 102, 20));
        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 337, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        MainWindowClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindowClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuFile->addSeparator();
        menuFile->addAction(actionFragment_Shader);
        menuFile->addAction(actionVertex_Shader);
        menuFile->addSeparator();
        menuFile->addAction(actionQuit);
        menuEdit->addAction(actionReset_Particles);

        retranslateUi(MainWindowClass);

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "Fluid Brush", 0));
        actionOpen->setText(QApplication::translate("MainWindowClass", "Open", 0));
        actionNew->setText(QApplication::translate("MainWindowClass", "New", 0));
        actionSave->setText(QApplication::translate("MainWindowClass", "Save", 0));
        actionFragment_Shader->setText(QApplication::translate("MainWindowClass", "Fragment Shader", 0));
        actionVertex_Shader->setText(QApplication::translate("MainWindowClass", "Vertex Shader", 0));
        actionReset_Particles->setText(QApplication::translate("MainWindowClass", "Reset Particles", 0));
        actionQuit->setText(QApplication::translate("MainWindowClass", "Quit", 0));
        MouseRadius->setText(QApplication::translate("MainWindowClass", "Stroke Width", 0));
        VelocityAmplifer->setText(QApplication::translate("MainWindowClass", "Turbulence Speed", 0));
        checkBoxPoints->setText(QApplication::translate("MainWindowClass", "Show points", 0));
        checkBoxTexture->setText(QApplication::translate("MainWindowClass", "Show texture", 0));
        stats->setText(QApplication::translate("MainWindowClass", "Stats", 0));
        groupBox->setTitle(QApplication::translate("MainWindowClass", "Color", 0));
        labelRed->setText(QApplication::translate("MainWindowClass", "Red", 0));
        labelBlue->setText(QApplication::translate("MainWindowClass", "Blue", 0));
        labelGreen->setText(QApplication::translate("MainWindowClass", "Green", 0));
        VelocityAmplifer_2->setText(QApplication::translate("MainWindowClass", "Directional Speed", 0));
        groupBox_2->setTitle(QApplication::translate("MainWindowClass", "Brush Type", 0));
        radioSmoke->setText(QApplication::translate("MainWindowClass", "Wispy", 0));
        radioBubbles->setText(QApplication::translate("MainWindowClass", "Bubbly", 0));
        radioStars->setText(QApplication::translate("MainWindowClass", "Starry", 0));
        menuFile->setTitle(QApplication::translate("MainWindowClass", "File", 0));
        menuEdit->setTitle(QApplication::translate("MainWindowClass", "Edit", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
