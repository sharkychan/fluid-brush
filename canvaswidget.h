#ifndef CANVASWIDGET_H
#define CANVASWIDGET_H

#include <QtWidgets>
#include <QtOpenGL>
#include <QGLShaderProgram>
#include <QGLWidget>
#include <QMouseEvent>
#include <QBasicTimer>
#include <QTimerEvent>
#include <QTabletEvent>

#include <QMenuBar>
#include <QMenu>
#include <QAction>

#include "velocitysystem.h"
#include "brushvectorfield.h"

#include "util.h"
#include <vector>

#include <QThread>

class OpenGLThread : public QThread {
    Q_OBJECT
public:
    OpenGLThread(QObject *parent):m_parent(parent){}
    void run();

private:
    QObject *m_parent;
};

class CanvasWidget : public QGLWidget {
    Q_OBJECT

public:
    CanvasWidget(QWidget * parent = 0);
    CanvasWidget(QString imageName, QWidget *parent = 0);
    ~CanvasWidget();

    VelocitySystem *vs;

    //Flags for interface
    int showTexture;
    int showPoints;

    //This is dumb but I don't wanna fix it atm :|
    ShaderSelection shaderSelection;
    QGLShaderProgram * currentShader;
    QGLShaderProgram smokeShader;
    QGLShaderProgram bubbleShader;
    QGLShaderProgram fireShader;
    QGLShaderProgram starShader;

    int stroke_count;

    int useBackground;
    QString backgroundImage;

    GLuint imageTexture;                 //Texture for the background image

    //Color picker
    unsigned char pixel[4];
    float red;
    float green;
    float blue;

    //Controls for saving frames
    int numFrames;              //Counter of frames during screencapping
    int currentFrame;           //Counter of frames during playback
    enum RecordSetting {RECORDING, PLAYBACK, SIMULATION};
    int recordSetting;
    int captureTimeStep;

    //Calculate FPS
    QTime * frameTimer;
    int frameCount;

    //Controls for testing strokes
    enum ControlSetting {PRESSURE, TILT, VELOCITY, PLAIN, ALL};
    int controlSetting;

    void initFluidSystem(int _width, int _height);
    void initCanvas();
    void loadImage(QString imageName);

    //Shader stuff
    bool loadVertexShader(QGLShaderProgram &shader, QString vert_shader);
    bool loadFragmentShader(QGLShaderProgram &shader, QString frag_shader);

    //Playback method
    void particlePlayback(int frame);


protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int _width, int _height);

    void tabletEvent(QTabletEvent *event);
    void timerEvent(QTimerEvent *event);

    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent * event);

private:
    int width;
    int height;

    Vec2f tilt;
    float tiltMagnitude;
    float pressure;
    Vec2f velocity;

    //Mouse controls
    enum ActiveButton {LEFT, RIGHT, MIDDLE, NONE};
    int activeButton;
    Vec2f cursorPos;

    //Tablet controls
    QTabletEvent::TabletDevice tablet;
    bool stylusDown;
    enum ActiveKey {SHIFT, CTRL, NOKEY};
    int activeKey;

    //Event timers
    QBasicTimer simulationTimer;
    QBasicTimer interpolationTimer;
    QBasicTimer playbackTimer;

    int sampleCount;

    //Interactivity methods
    void updateBrush(QTabletEvent *event);

    //Touch Strokes
    void beginStroke(int x, int y);
    void continueStroke(int x, int y);
    bool shouldGeneratePoint(const Vec2f &position, const Stroke &s);
    void endStroke();

    std::vector<Touches *> createCubicSubdivisions(const Touches &p0, const Touches &p1, const Touches &p2, const Touches &p3);
    std::vector<Touches *> createCosineSubdivisions(const Touches &p0, const Touches &p1);

    Vec2f cubicInterpolation(const Vec2f &p0, const Vec2f &p1, const Vec2f &p2, const Vec2f &p3, float mu);
    Vec2f cosineInterpolation(const Vec2f &p0, const Vec2f &p1, float mu);
    float cubicInterpolation(float p0, float p1, float p2, float p3, float mu);
    float cosineInterpolation(float p0, float p1, float mu);

    //Rendering methods
    void displayImage();
    void renderParticles();
    void renderPoints();
    void texDraw(Vec2f point, float radius, float angle);
    void drawCircle(float cx, float cy, float cz, float r, int num_segments);
    void drawBrushHeatMap();

    //Set parameter methods
    float findDensity();
    double findRadius();
    float findTurbulenceAmplifier();
    float findDirectionalAmplifier();
    void setShader();

    float logisticsCurve(float x);
};

#endif // CANVASWIDGET_H
