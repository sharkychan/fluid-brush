#include "tabletapplication.h"

 bool TabletApplication::event(QEvent *event) {
     if (event->type() == QEvent::TabletEnterProximity) {
         //printf("Tablet Enter\n");
         return true;
     } else if (event->type() == QEvent::TabletLeaveProximity) {
         //printf("Tablet Leave\n");
         return true;
     }
     return QApplication::event(event);
 }
