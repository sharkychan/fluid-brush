#ifndef FLUIDVECTORFIELD_H
#define FLUIDVECTORFIELD_H

#include "vec.h"

#include "FastPerlinNoise.h"
#include "Touches.h"

class FluidVectorField {
public:
    int window_width;
    int window_height;

    //value that interpolates between 2 textures
    float interpVal;

    float turbulence_amplifier;

    float persistence;
    float octaves;

    float delta_x;

    FastPerlinNoise *fastpn;

    FluidVectorField(int width, int height);

    float interpNoise(float x, float y);
    float potential(float x, float y, const Touches& touch);
    Vec2f get_velocity(const Vec2f& x, const Touches& touch, float amplifier);

    void incrementInterp();
};

#endif // FLUIDVECTORFIELD_H
