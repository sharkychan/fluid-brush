#include "connectorvariable.h"

ConnectorVariable::ConnectorVariable(QObject* parent):QObject(parent) {
    m_value = 0.0;
}

int ConnectorVariable::value() {
    return m_value;
}

void ConnectorVariable::setValue(int value) {
    if (value != m_value) {
        m_value = value;
        emit valueChanged(value/10.0);
    }
}

void ConnectorVariable::valueChanged(double newValue) {

}
