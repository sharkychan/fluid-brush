/*
 *  FastPerlinNoise.h
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 12/3/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

//Based on Perlin's updated Noise Generation for Java

#ifndef _FASTPERLINNOISE
#define _FASTPERLINNOISE

class FastPerlinNoise {
        int half_n;
        int n;
        int *permut;
        int *permut1;
        int *permut2;


public:
        FastPerlinNoise();
        ~FastPerlinNoise();
        float fade(float t);
        float grad(int hash, float x, float y);
        float lerp(float t, float a, float b);
        float noise(float x, float y);
        float noise1(float x, float y);
        float noise2(float x, float y);


        void shufflePermut();
        void shufflePermut1();
        void shufflePermut2();

        void switchPermuts();

        float perlinNoise(float x, float y, float persistence, int octaves);
        float * createNoiseTexture(int width, int height, float persistence, int octaves);
};
#endif
