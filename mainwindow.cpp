#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "canvaswidget.h"

#include <QtWidgets>
#include <QtOpenGL>

#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QImage>
#include <QString>
#include <string>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindowClass)
{
    ui->setupUi(this);

    //Set up the QGraphicsView
    colorScene = new QGraphicsScene(ui->colorDisplay->sceneRect());
    red = 100;
    green = 100;
    blue = 100;
    updateColor(red, green, blue);

    //Set up shader selection
    ui->radioSmoke->setChecked(true);
    shader = ShaderSelection::SMOKE;

    connect(ui->radioSmoke, SIGNAL(toggled(bool)), this, SLOT(toggleSmoke()));
    connect(ui->radioBubbles, SIGNAL(toggled(bool)), this, SLOT(toggleBubble()));
    connect(ui->radioStars, SIGNAL(toggled(bool)), this, SLOT(toggleStar()));

    //Set up short cuts to menu items
    ui->actionNew->setShortcut(QKeySequence(QString("Ctrl+n")));
    ui->actionOpen->setShortcut(QKeySequence(QString("Ctrl+o")));
    ui->actionSave->setShortcut(QKeySequence(QString("Ctrl+s")));
    ui->actionQuit->setShortcut(QKeySequence(QString("Ctrl+q")));

    ui->actionReset_Particles->setShortcut(QKeySequence(QString("Ctrl+r")));
    ui->actionFragment_Shader->setShortcut(QKeySequence(QString("Ctrl+f")));
    ui->actionVertex_Shader->setShortcut(QKeySequence(QString("Ctrl+v")));

    //Set up the check boxes
    connect(ui->checkBoxPoints, SIGNAL(clicked(bool)), this, SLOT(togglePoints(bool)));
    connect(ui->checkBoxTexture, SIGNAL(clicked(bool)), this, SLOT(toggleTexture(bool)));
    ui->checkBoxTexture->setChecked(true);

    //Variables associated with slider values
    min_radius = 5;
    radius = 20;
    turbulence_amplifier = 2000;
    directional_amplifier = 5;

    //Set up all the slider values
    //Stroke Radius
    ui->sliderRadius->setRange(min_radius, 200);
    ui->sliderRadius->setSingleStep(1);
    ui->sliderRadius->setValue(radius);
    connect(ui->sliderRadius, SIGNAL(valueChanged(int)), this, SLOT(on_radius_updated(int)));

    //Set up color sliders
    ui->sliderRed->setRange(0, 255);
    ui->sliderRed->setSingleStep(1);
    ui->sliderRed->setValue(red);
    connect(ui->sliderRed, SIGNAL(valueChanged(int)), this, SLOT(on_red_updated(int)));

    ui->sliderGreen->setRange(0, 255);
    ui->sliderGreen->setSingleStep(1);
    ui->sliderGreen->setValue(green);
    connect(ui->sliderGreen, SIGNAL(valueChanged(int)), this, SLOT(on_green_updated(int)));

    ui->sliderBlue->setRange(0, 255);
    ui->sliderBlue->setSingleStep(1);
    ui->sliderBlue->setValue(blue);
    connect(ui->sliderBlue, SIGNAL(valueChanged(int)), this, SLOT(on_blue_updated(int)));

    //Turbulence Amplifier
    ui->sliderTurbulenceAmp->setRange(0, 7000);
    ui->sliderTurbulenceAmp->setSingleStep(100);
    ui->sliderTurbulenceAmp->setValue(turbulence_amplifier);
    connect(ui->sliderTurbulenceAmp, SIGNAL(valueChanged(int)), this, SLOT(on_turbulenceAmplifier_updated(int)));

    //Directional Amplifier
    ui->sliderDirectionalAmp->setRange(0, 15);
    ui->sliderDirectionalAmp->setSingleStep(1);
    ui->sliderDirectionalAmp->setValue(directional_amplifier);
    connect(ui->sliderDirectionalAmp, SIGNAL(valueChanged(int)), this, SLOT(on_directionalAmplifier_updated(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

//Reset button currently resets all fluid simulations
void MainWindow::on_reset_clicked() {
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->vs->resetParticles();
        (*canvas)->vs->bvf->initializeVectorField();
    }
}

//Exit the program
void MainWindow::on_quit_clicked() {
    qApp->quit();
}

void MainWindow::on_stats_clicked() {
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->vs->printState();
    }
}


void MainWindow::togglePoints(bool showPoints) {
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->showPoints = showPoints;
    }
}

void MainWindow::toggleTexture(bool showTexture) {
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->showTexture = showTexture;
    }
}

//Radio buttons for shaders

void MainWindow::toggleSmoke() {
    shader = ShaderSelection::SMOKE;
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->shaderSelection = shader;
    }
}

void MainWindow::toggleBubble() {
    shader = ShaderSelection::BUBBLE;
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->shaderSelection = shader;
    }
}

void MainWindow::toggleFire() {
    shader = ShaderSelection::FIRE;
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->shaderSelection = shader;
    }
}

void MainWindow::toggleStar() {
    shader = ShaderSelection::STAR;
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->shaderSelection = shader;
    }
}

//Slots for the various sliders

void MainWindow::on_radius_updated(int _radius) {
    radius = (float)_radius;
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->vs->mouse_radius = radius;
    }
}

void MainWindow::on_turbulenceAmplifier_updated(int _turbulence_amplifier) {
    turbulence_amplifier = (float)_turbulence_amplifier;
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->vs->fvf->turbulence_amplifier = turbulence_amplifier;
    }
}

void MainWindow::on_directionalAmplifier_updated(int _directional_amplifier) {
    directional_amplifier = (float)_directional_amplifier;
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->vs->bvf->directional_amplifier = directional_amplifier;
    }
}


void MainWindow::on_red_updated(int _red) {
    red = _red;
    currentColor = QColor::fromRgb(red, green, blue);
    colorScene->setBackgroundBrush(currentColor);

    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->red = red/255.0;
    }
}

void MainWindow::on_green_updated(int _green) {
    green = _green;
    currentColor = QColor::fromRgb(red, green, blue);
    colorScene->setBackgroundBrush(currentColor);

    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->green = green/255.0;
    }
}

void MainWindow::on_blue_updated(int _blue) {
    blue = _blue;
    currentColor = QColor::fromRgb(red, green, blue);
    colorScene->setBackgroundBrush(currentColor);

    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->blue = blue/255.0;
    }
}

//Create a blank canvas for experimentation with fluid effects
void MainWindow::on_actionNew_triggered()
{
    initializeView(600, 800, 0, QString());
}

//Open an image to add fluid effects
void MainWindow::on_actionOpen_triggered() {
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"), QDir::homePath());
    if (!fileName.isEmpty()) {
        QImage image(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, tr("Fluid Brush"),
                                     tr("Cannot load %1.").arg(fileName));
            return;
        }

        initializeView(image.width(), image.height(), 1, fileName);
    }
}

//Save out a series of images to create an animated gif
void MainWindow::on_actionSave_triggered() {
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->numFrames = 0;
    }
}

void MainWindow::on_actionQuit_triggered() {
    qApp->quit();
}

void MainWindow::on_actionReset_Particles_triggered() {
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->vs->resetParticles();
        (*canvas)->vs->bvf->initializeVectorField();
    }
}

//TODO: generalize this path...
void MainWindow::on_actionFragment_Shader_triggered() {
    QString path = QDir::homePath() + QString("/Desktop/Fluid Brush/shaders/");
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Fragment Shader"), path);
    if (!fileName.isEmpty()) {

    }
}

//TODO: generalize this path...
void MainWindow::on_actionVertex_Shader_triggered() {
    QString path = QDir::homePath() + QString("/Desktop/Fluid Brush/shaders/");
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Vertex Shader"), path);
    if (!fileName.isEmpty()) {

    }
}

void MainWindow::initializeView(int _width, int _height, int loadImage, QString imageName) {
    CanvasWidget *canvas;
    if (loadImage) {
        canvas = new CanvasWidget(imageName);
    } else {
        canvas = new CanvasWidget();
    }
    //OpenGLThread* m_processThread = new OpenGLThread(this);

    canvas->resize(_width, _height);
    canvas->initFluidSystem(_width, _height);
    canvases.push_back(canvas);

    //Set fluid system values based on current gui parameters
    updateFluidControls();
    canvases[canvases.size()-1]->show();
}

//Update controls before opening drawing widget
void MainWindow::updateFluidControls() {
    std::vector<CanvasWidget *>::iterator canvas;
    for (canvas = canvases.begin(); canvas != canvases.end(); canvas++) {
        (*canvas)->showTexture = ui->checkBoxTexture->isChecked() ? 1 : 0;
        (*canvas)->showPoints = ui->checkBoxPoints->isChecked() ? 1 : 0;

        (*canvas)->vs->min_radius = min_radius;
        (*canvas)->vs->mouse_radius = radius;

        (*canvas)->vs->fvf->turbulence_amplifier = turbulence_amplifier;
        (*canvas)->vs->bvf->directional_amplifier = directional_amplifier;

        (*canvas)->red = red/255.0;
        (*canvas)->green = green/255.0;
        (*canvas)->blue = blue/255.0;

        (*canvas)->shaderSelection = shader;
    }
}

void MainWindow::updateColor(int _red, int _green, int _blue) {
    red = _red;
    green = _green;
    blue = _blue;
    ui->sliderRed->setValue(red);
    ui->sliderGreen->setValue(green);
    ui->sliderBlue->setValue(blue);

    currentColor = QColor::fromRgb(red, green, blue);
    colorScene->setBackgroundBrush(currentColor);
    ui->colorDisplay->setScene(colorScene);
}

