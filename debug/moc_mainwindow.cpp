/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Tue May 6 13:34:24 2014
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      37,   11,   11,   11, 0x08,
      63,   11,   11,   11, 0x08,
      95,   89,   11,   11, 0x08,
     127,  116,   11,   11, 0x08,
     158,  146,   11,   11, 0x08,
     178,   11,   11,   11, 0x08,
     197,   11,   11,   11, 0x08,
     215,   11,   11,   11, 0x08,
     242,  234,   11,   11, 0x08,
     285,  265,   11,   11, 0x08,
     324,  319,   11,   11, 0x08,
     351,  344,   11,   11, 0x08,
     379,  373,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0on_actionNew_triggered()\0"
    "on_actionOpen_triggered()\0"
    "on_actionSave_triggered()\0apply\0"
    "applySmoothing(bool)\0showPoints\0"
    "togglePoints(bool)\0showTexture\0"
    "toggleTexture(bool)\0on_reset_clicked()\0"
    "on_quit_clicked()\0on_stats_clicked()\0"
    "_radius\0on_radius_updated(int)\0"
    "_velocity_amplifier\0"
    "on_velocityAmplifier_updated(int)\0"
    "_red\0on_red_updated(int)\0_green\0"
    "on_green_updated(int)\0_blue\0"
    "on_blue_updated(int)\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_actionNew_triggered(); break;
        case 1: on_actionOpen_triggered(); break;
        case 2: on_actionSave_triggered(); break;
        case 3: applySmoothing((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: togglePoints((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: toggleTexture((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: on_reset_clicked(); break;
        case 7: on_quit_clicked(); break;
        case 8: on_stats_clicked(); break;
        case 9: on_radius_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: on_velocityAmplifier_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: on_red_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: on_green_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: on_blue_updated((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
