#ifndef GLSCENE_H
#define GLSCENE_H

#include <QGraphicsScene>
#include <QTimer>
#include "fluidsystem.h"
#include "canvaswidget.h"
#include "vec.h"


class GLScene : public QGraphicsScene {
    Q_OBJECT

public:
    GLScene(int width, int height);
    ~GLScene();

    void initGL();
    void startRefresh();
    void drawBackground(QPainter *painter, const QRectF &rect);
    void drawForeground(QPainter *painter, const QRectF &rect);
    void drawTexture(Vec2f point, float radius, float alpha);
    void connectToCanvas(CanvasWidget *_cw);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void wheelEvent(QGraphicsSceneWheelEvent * wheelEvent);

private:
    QTimer *refreshTimer;
    QTimer *simulationTimer;
    QTimer *interpolationTimer;

    int window_width;
    int window_height;
    FluidSystem *fs;
    CanvasWidget *cw;

    //Mouse controls
    enum ActiveButton {LEFT, RIGHT, MIDDLE, NONE};
    int activeButton;

    //Flags for interface
    int showTexture;
    int showPoints;

    void drawCircle(float cx, float cy, float cz, float r, int num_segments);

private slots:
    void refresh();
    void simStep();
    void interpStep();

};

#endif // GLSCENE_H
