#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QtWidgets>
#include <QtOpenGL>
#include <QMainWindow>
#include "canvaswidget.h"
#include "util.h"

#include <vector>

namespace Ui
{
    class MainWindowClass;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int red;
    int blue;
    int green;

    ShaderSelection shader;

private:
    Ui::MainWindowClass *ui;
    std::vector<CanvasWidget *> canvases;

    void initializeView(int _width, int _height, int loadImage, QString imageName);
    void updateFluidControls();
    void updateColor(int _red, int _green, int _blue);

    float min_radius;
    float radius;
    float turbulence_amplifier;
    float directional_amplifier;

    QGraphicsScene * colorScene;
    QColor currentColor;

private slots:
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionQuit_triggered();

    void on_actionReset_Particles_triggered();
    void on_actionFragment_Shader_triggered();
    void on_actionVertex_Shader_triggered();

    void togglePoints(bool showPoints);
    void toggleTexture(bool showTexture);

    void toggleSmoke();
    void toggleBubble();
    void toggleFire();
    void toggleStar();

    void on_reset_clicked();
    void on_quit_clicked();
    void on_stats_clicked();

    void on_radius_updated(int _radius);
    void on_turbulenceAmplifier_updated(int _turbulence_amplifier);
    void on_directionalAmplifier_updated(int _directional_amplifier);

    void on_red_updated(int _red);
    void on_green_updated(int _green);
    void on_blue_updated(int _blue);

};

#endif // MAINWINDOW_H
