#include "canvaswidget.h"

#include <QImage>
#include <QCursor>

CanvasWidget::CanvasWidget(QWidget *parent):
    QGLWidget(parent) {
    initCanvas();

    useBackground = 0;
}

CanvasWidget::CanvasWidget(QString imageName, QWidget *parent):
    QGLWidget(parent) {
    initCanvas();

    backgroundImage = imageName;
    useBackground = 1;
}


CanvasWidget::~CanvasWidget() {
    delete vs;
    delete frameTimer;
}

//Initialize the Canvas
void CanvasWidget::initCanvas() {
    setMouseTracking(true);
    activeButton = NONE;
    activeKey = NOKEY;
    tablet = QTabletEvent::Stylus;
    stylusDown = false;

    sampleCount = 0;

    tilt = Vec<2, float> (0.0, 0.0);
    tiltMagnitude = 0.0;
    pressure = 0.0;
    velocity = Vec<2, float> (0.0, 0.0);

    frameCount = 0;
    frameTimer = new QTime();
    frameTimer->start();

    numFrames = -1;
    currentFrame = 0;
    recordSetting = SIMULATION;
    captureTimeStep = 120;   //this is half of the desired length of captured frames

    controlSetting = ALL;

    stroke_count = 0;
}


//Initialize OpenGL and bind shaders
void CanvasWidget::initializeGL() {
    glClearColor(1.0, 1.0, 1.0, 1.0);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Initialize shaders
    loadVertexShader(smokeShader, QString(":/shaders/smoke.vert"));
    loadFragmentShader(smokeShader, QString(":/shaders/smoke.frag"));

    loadVertexShader(bubbleShader, QString(":/shaders/bubble.vert"));
    loadFragmentShader(bubbleShader, QString(":/shaders/bubble.frag"));

    loadVertexShader(fireShader, QString(":/shaders/spiky.vert"));
    loadFragmentShader(fireShader, QString(":/shaders/spiky.frag"));

    loadVertexShader(starShader, QString(":/shaders/star.vert"));
    loadFragmentShader(starShader, QString(":/shaders/star.frag"));

    currentShader = &smokeShader;

    //Load image here
    if (useBackground) {
        loadImage(backgroundImage);
    }
}

//Initialize FluidSystem and UI
void CanvasWidget::initFluidSystem(int _width, int _height) {
    vs = new VelocitySystem(_width, _height);
    vs->currentTouchStroke = new Stroke(0.1, 0); //0.1 is initial_alpha -- TODO: refactor for clarity

    simulationTimer.start(16, this);
    interpolationTimer.start(500, this);

    showTexture = 1;
    showPoints = 0;
}

//Resizing the canvas means resizing the fluid system, so this will break everything!
void CanvasWidget::resizeGL(int _width, int _height) {
    width = _width;
    height = _height;

    glViewport(0, 0, _width, _height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, _width, 0, _height, 1, 0);
    glMatrixMode(GL_MODELVIEW);
}

//Paint function
void CanvasWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    frameCount++;

    glLoadIdentity();

    //If an image was loaded in, display it
    if (useBackground) {
        displayImage();
    }

    if (showPoints) {
        //Draw vector field heatmap
        drawBrushHeatMap();
    }

    //Render fluid particles according to SIMULATION or PLAYBACK accordingly
    renderParticles();

    //Render points
    if (showPoints) {
        renderPoints();
    }

    //Show brush size and position if the cursor is on the screen
    if (underMouse()) {
        if (stylusDown) {
            glUseProgramObjectARB(0);
            glColor4f(0.7, 0.7, 0.7, 0.3);
            drawCircle(cursorPos[0], cursorPos[1], 0.0, vs->mouse_radius/2.0, 16);
        }
    }
}

//Load, compile and bind the vertex shader
bool CanvasWidget::loadVertexShader(QGLShaderProgram &shader, QString vert_shader) {
    if (!shader.addShaderFromSourceFile(QGLShader::Vertex, vert_shader)) return false;
    if (!shader.link()) return false;

    return true;
}

//Load, compile and bind the fragment shader
bool CanvasWidget::loadFragmentShader(QGLShaderProgram &shader, QString frag_shader) {
    if (!shader.addShaderFromSourceFile(QGLShader::Fragment, frag_shader)) return false;
    if (!shader.link()) return false;

    return true;
}

//Generate image as a texture and load it in
void CanvasWidget::loadImage(QString imageName) {
    imageTexture = bindTexture(QPixmap(imageName), GL_TEXTURE_2D);
}

//Display the image as a texture
void CanvasWidget::displayImage() {
    glColor4f(1.0, 1.0, 1.0, 1.0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, imageTexture);

    glBegin(GL_QUADS);
    glTexCoord2d(0.0, 1.0); glVertex3d(0, height, 0);      //Upper left corner
    glTexCoord2d(1.0, 1.0); glVertex3d(width, height, 0);       //Upper Right corner
    glTexCoord2d(1.0, 0.0); glVertex3d(width, 0, 0);        //Lower Right corner
    glTexCoord2d(0.0, 0.0); glVertex3d(0, 0, 0);       //Lower Left corner
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

//Process tablet events
void CanvasWidget::tabletEvent(QTabletEvent *event) {
    QGLWidget::tabletEvent(event);
    QPointF position = event->pos();
    int x = position.x();
    int y = height-position.y();

    switch (event->type()) {
    case QEvent::TabletPress:
        if (!stylusDown) {
            stylusDown = true;
            if (event->pointerType() == QTabletEvent::Pen) {
                activeButton = LEFT;
                if (activeKey == NOKEY) {
                    beginStroke(x, y);
                } else if (activeKey == CTRL) {
                    //Get the color at the given point
                    //TODO: send pixel colors back to GUI display!
                    glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
                    red = (float) pixel[0]/255.0;
                    green = (float) pixel[1]/255.0;
                    blue = (float) pixel[2]/255.0;

                    Vec2f position = Vec<2, float> (x, y);
                    Vec2f velocity = vs->bvf->getCellVelocity(position);
                }
            } else if (event->pointerType() == QTabletEvent::Eraser) {
                activeButton = RIGHT;
                //vs->eraseTouches(x, y);
                vs->eraseParticles(x, y);
            }
        }
        break;
    case QEvent::TabletRelease:
        if (stylusDown) {
            stylusDown = false;
            activeButton = NONE;
            if (event->pointerType() == QTabletEvent::Pen) {
                if (activeKey == NOKEY) {
                    endStroke();
                }
            }
        }
        break;
    case QEvent::TabletMove:
        //Track all mouse positions
        cursorPos = Vec<2, float> ((float)x, (float)y);
        if (stylusDown) {
            updateBrush(event);
            if (event->pointerType() == QTabletEvent::Pen) {
                if (activeKey == NOKEY) {
                    continueStroke(x, y);
                }
            } else if (event->pointerType() == QTabletEvent::Eraser) {
                //vs->eraseTouches(x, y);
                vs->eraseParticles(x, y);
            }
        }
        break;
    default:
        break;
    }
}

void CanvasWidget::keyPressEvent(QKeyEvent * event) {
    if (event->key() == Qt::Key_Control) {
        activeKey = CTRL;
    }


    if (event->key() == Qt::Key_P) {
        controlSetting = PRESSURE;
    }
    if (event->key() == Qt::Key_T) {
        controlSetting = TILT;
    }
    if (event->key() == Qt::Key_V) {
        controlSetting = VELOCITY;
    }
    if (event->key() == Qt::Key_N) {
        controlSetting = PLAIN;
    }
    if (event->key() == Qt::Key_A) {
        controlSetting = ALL;
    }

}

void CanvasWidget::keyReleaseEvent(QKeyEvent * event) {
    activeKey = NOKEY;
}

void CanvasWidget::updateBrush(QTabletEvent *event) {
    //Ranges between -60 and 60
    tilt[0] = (float) event->xTilt();
    tilt[1] = (float) event->yTilt();
    tiltMagnitude = mag(tilt)/60.0;
    pressure = event->pressure();    //Ranges between 0 and 1.0
}

//User initiates creation of a stroke -- start with points rather than touches
void CanvasWidget::beginStroke(int x, int y) {
    setShader();
    Vec2f position = Vec<2, float> ((float) x, (float) y);
    stroke_count++;
    vs->currentTouchStroke = new Stroke(vs->initial_alpha, stroke_count);
    Touches * t = new Touches(position, vs->noise_gain, vs->noise_lengthscale);
    t->setTouchParameters(findDensity(), findRadius(), findTurbulenceAmplifier(), findDirectionalAmplifier());
    t->setColor(red, green, blue);
    t->setIndex(0);

    //Push back initial touch as a point and as a touch
    vs->currentTouchStroke->points.push_back(t);
    vs->currentTouchStroke->touches.push_back(t);
}

//As points are added, generate touches and necessary subdivisions
void CanvasWidget::continueStroke(int x, int y) {
    Vec2f position = Vec<2, float> ((float) x, (float) y);

    //Increment sample point count
    sampleCount++;

    //Check whether points are far enough apart to generate the next point
    if (shouldGeneratePoint(position, *vs->currentTouchStroke)) {
        Touches * t = new Touches(position, vs->noise_gain, vs->noise_lengthscale);
        Touches * prevTouch = vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-1);
        t->setTouchParameters(findDensity(), findRadius(), findTurbulenceAmplifier(), findDirectionalAmplifier());
        t->setColor(red, green, blue);

        //Set velocity
        t->setVelocity(prevTouch, sampleCount);
        velocity = t->velocity;

        prevTouch->generateOuterPoints(t->position);

        //Reset sampleCount
        sampleCount = 0;

        //If this is the second touch, set the first touch's velocity to this touch's velocity
        if (vs->currentTouchStroke->points.size() == 1) {
            vs->currentTouchStroke->points.at(0)->velocity = t->velocity;
        }

        //Push back touch as a point
        vs->currentTouchStroke->points.push_back(t);

        Touches * nextTouch;
        std::vector<Touches *> subdivs;
        if (vs->currentTouchStroke->points.size() == 2) {
            //Do cosine interpolation between the points 0 and 1
            Touches * t0 = vs->currentTouchStroke->points.at(0);
            Touches * t1 = vs->currentTouchStroke->points.at(1);

            //Push back the first touch into the vector of touches
            vs->generateParticles(*vs->currentTouchStroke, *t0, *t1, vs->initial_alpha, *currentShader);
            subdivs = createCosineSubdivisions(*t0, *t1);
            nextTouch = t1;
        } else if (vs->currentTouchStroke->points.size() >= 4) {
            //Do cubic interpolation between points 2 and onward
            Touches * t3 = vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-1);
            Touches * t2 = vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-2);
            Touches * t1 = vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-3);
            Touches * t0 = vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-4);

            t1->generateOuterPoints(t0->position);

            //Push back the the touch that's two back into the vector of touches
            int index = vs->currentTouchStroke->touches.size();
            t1->setIndex(index);
            vs->currentTouchStroke->touches.push_back(t1);

            subdivs = createCubicSubdivisions(*t0, *t1, *t2, *t3);
            nextTouch = t2;
        }


        //Generate particles within the subdivisions
        std::vector<Touches *>::iterator subdiv;
        Touches * prevsubdiv = vs->currentTouchStroke->touches.at(vs->currentTouchStroke->touches.size()-1);
        for (subdiv = subdivs.begin(); subdiv != subdivs.end(); ++subdiv) {
            vs->generateParticles(*vs->currentTouchStroke, *prevsubdiv, *(*subdiv), vs->initial_alpha, *currentShader);
            prevsubdiv = (*subdiv);
        }

        //Insert the subdivisions into the overall stroke
        vs->currentTouchStroke->touches.insert(vs->currentTouchStroke->touches.end(), subdivs.begin(), subdivs.end());

        if (vs->currentTouchStroke->points.size() > 4) {
            vs->generateParticles(*vs->currentTouchStroke, *prevsubdiv, *nextTouch, vs->initial_alpha, *currentShader);
        }
    } else {
        //Generate "temp" touches to create particles without adding to the stroke
        std::auto_ptr<Touches> t(new Touches(position, vs->noise_gain, vs->noise_lengthscale));
        Touches * prevTouch = vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-1);
        t->setTouchParameters(findDensity(), findRadius(), findTurbulenceAmplifier(), findDirectionalAmplifier());
        t->generateOuterPoints(prevTouch->position);
        vs->generateParticles(*vs->currentTouchStroke, *prevTouch, *t.get(), vs->initial_alpha, *currentShader);
    }
}

//Stroke is finished, so wrap up the tail end of it
void CanvasWidget::endStroke() {
    if (vs->currentTouchStroke->points.size() == 1) {
        //Increase the radius for a single touch and generate its particles
        Touches * t = vs->currentTouchStroke->touches.at(0);
        t->radius *= 3;
        vs->generateParticles(*vs->currentTouchStroke, *t, vs->initial_alpha, *currentShader);
    } else if (vs->currentTouchStroke->points.size() == 2) {
        //If there are two points, create touch at last point
        Touches * t = new Touches(vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-1));
        vs->currentTouchStroke->touches.push_back(t);
        t->setIndex(2);
        //TODO: generate particles for these touches
    } else {
        //If there are more than two points, create subdivided touches along the last two points
        Touches * t0 = new Touches(vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-2));
        Touches * t1 = new Touches(vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-1));
        t0->generateOuterPoints(t1->position);
        t1->generateOuterPoints(t0->position);

        int index = vs->currentTouchStroke->touches.size();
        t0->setIndex(index);
        vs->currentTouchStroke->touches.push_back(t0);

        std::vector<Touches *> subdivs = createCosineSubdivisions(*t0, *t1);
        vs->currentTouchStroke->touches.insert(vs->currentTouchStroke->touches.end(), subdivs.begin(), subdivs.end());

        std::vector<Touches *>::iterator subdiv;
        Touches * prevsubdiv = t0;
        for (subdiv = subdivs.begin(); subdiv != subdivs.end(); ++subdiv) {
            vs->generateParticles(*vs->currentTouchStroke, *prevsubdiv, *(*subdiv), vs->initial_alpha, *currentShader);
            prevsubdiv = (*subdiv);
        }

        index = vs->currentTouchStroke->touches.size();
        t1->setIndex(index);
        vs->currentTouchStroke->touches.push_back(t1);
    }

    //Pass along this stroke's velocity to the brush velocity field and end it
    vs->bvf->updateVectorField(*vs->currentTouchStroke);
    vs->strokes.push_back(vs->currentTouchStroke);
    vs->currentTouchStroke->points.clear();
}

//Determine whether to generate a new touch on the canvas
bool CanvasWidget::shouldGeneratePoint(const Vec2f &position, const Stroke &s) {
    //If this is not the first point
    if (!s.points.empty()) {
        Touches * prevPosition = vs->currentTouchStroke->points.at(vs->currentTouchStroke->points.size()-1);

        if (dist(position, prevPosition->position) > findRadius()*.75) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

//Create potential subdivisions along stroke using cubic interpolation
std::vector<Touches *> CanvasWidget::createCubicSubdivisions(const Touches &p0, const Touches &p1, const Touches &p2, const Touches &p3) {
    std::vector<Touches *> subdivs;
    int index = vs->currentTouchStroke->touches.size();
    int stepnum = floor(dist(p1.position, p2.position)/(p1.radius));
    float step = 1.0/stepnum;
    for (float i = step; i < 1.0; i = i+step) {
        //Add in subdivided points along spline
        Vec2f point = cubicInterpolation(p0.position, p1.position, p2.position, p3.position, i);
        Touches * t = new Touches(point, p0.noise_gain, p0.noise_length);
        t->setIndex(index);
        index++;

        if (i == step) {
            t->generateOuterPoints(p0.position);
        } else {
            t->generateOuterPoints(subdivs.at(subdivs.size()-1)->position);
        }

        float radius = cubicInterpolation(p0.radius, p1.radius, p2.radius, p3.radius, i);
        float density = p0.density;
        float turbulence = cubicInterpolation(p0.turbulence_amplifier, p1.turbulence_amplifier,
                                                     p2.turbulence_amplifier, p3.turbulence_amplifier, i);
        float directionality = cubicInterpolation(p0.directional_amplifier, p1.directional_amplifier,
                                                      p2.directional_amplifier, p3.directional_amplifier, i);
        t->setTouchParameters(density, radius, turbulence, directionality);
        t->setColor(red, green, blue);
        t->velocity = cubicInterpolation(p0.velocity, p1.velocity, p2.velocity, p3.velocity, i);
        subdivs.push_back(t);
    }

    return subdivs;
}

std::vector<Touches *> CanvasWidget::createCosineSubdivisions(const Touches &p0, const Touches &p1) {
    std::vector<Touches *> subdivs;
    int index = vs->currentTouchStroke->touches.size();
    int stepnum = floor(dist(p0.position, p1.position)/(p0.radius));
    float step = 1.0/stepnum;
    for (float i = step; i < 1.0; i = i+step) {
        Vec2f point = cosineInterpolation(p0.position, p1.position, i);
        Touches * t = new Touches(point, p0.noise_gain, p0.noise_length);
        t->setIndex(index);
        index++;

        if (i == step) {
            t->generateOuterPoints(p0.position);
        } else {
            t->generateOuterPoints(subdivs.at(subdivs.size()-1)->position);
        }

        float radius = cosineInterpolation(p0.radius, p1.radius, i);
        float density = p0.density;
        float turbulence = cosineInterpolation(p0.turbulence_amplifier, p1.turbulence_amplifier, i);
        float directionality = cosineInterpolation(p0.directional_amplifier, p1.directional_amplifier, i);
        t->setTouchParameters(density, radius, turbulence, directionality);
        t->setColor(red, green, blue);
        t->velocity = cosineInterpolation(p0.velocity, p1.velocity, i);
        subdivs.push_back(t);
    }

    return subdivs;
}

//Cubic interpolation between points p1 and p2 at position mu along line
//TODO: combine this into template to allow vec2f and float?
Vec2f CanvasWidget::cubicInterpolation(const Vec2f &p0, const Vec2f &p1, const Vec2f &p2, const Vec2f &p3, float mu) {
    Vec2f position = Vec<2, float> (0., 0.);

    float mu2 = mu*mu;

    float a0 = p3[0] - p2[0] - p0[0] + p1[0];
    float a1 = p0[0] - p1[0] - a0;
    float a2 = p2[0] - p0[0];
    float a3 = p1[0];

    position[0] = a0*mu*mu2 + a1*mu2 + a2*mu + a3;

    a0 = p3[1] - p2[1] - p0[1] + p1[1];
    a1 = p0[1] - p1[1] - a0;
    a2 = p2[1] - p0[1];
    a3 = p1[1];

    position[1] = a0*mu*mu2 + a1*mu2 + a2*mu + a3;

    return position;
}

Vec2f CanvasWidget::cosineInterpolation(const Vec2f &p0, const Vec2f &p1, float mu) {
    Vec2f position = Vec<2, float> (0., 0.);

    float mu2 = (1-cosf(mu*M_PI))/2;

    position[0] = p0[0]*(1-mu2) + p1[0]*mu2;
    position[1] = p0[1]*(1-mu2) + p1[1]*mu2;

    return position;
}

float CanvasWidget::cubicInterpolation(float p0, float p1, float p2, float p3, float mu) {
    float position = 0.0;

    float mu2 = mu*mu;

    float a0 = p3 - p2 - p0 + p1;
    float a1 = p0 - p1 - a0;
    float a2 = p2 - p0;
    float a3 = p1;

    position = a0*mu*mu2 + a1*mu2 + a2*mu + a3;

    return position;
}

float CanvasWidget::cosineInterpolation(float p0, float p1, float mu) {
    float position = 0.0;

    float mu2 = (1-cosf(mu*M_PI))/2;

    position = p0*(1-mu2) + p1*mu2;

    return position;
}

//Check timer for both simulation step and interpolation step
void CanvasWidget::timerEvent(QTimerEvent *event) {
    if (event->timerId() == simulationTimer.timerId()) {
        //Simulate particle movement
        vs->moveParticles();
        update();
        if (numFrames >= 0) {
            if (numFrames == 0) {
                vs->createParticleStates();
            }
            if (numFrames <= captureTimeStep) {
                //Record current particles
                vs->storeCurrentState();
                numFrames++;

            } else {
                //Enter playback mode
                recordSetting = PLAYBACK;

                //Loop particle values so larger loop appears seamless
                vs->adjustCurrentState();

                //Clean up
                numFrames = -1;
                simulationTimer.stop();
                playbackTimer.start(20, this);
            }
        }
    } else if (event->timerId() == playbackTimer.timerId()) {
        //Play back captured particle movements
        update();

        if (currentFrame > 0) {
            QImage image = grabFrameBuffer(true);
            QString output;
            if (currentFrame < 10) {
                output = QString("/Users/sharky/Desktop/screens/output00%1.png").arg(currentFrame);
            } else if (currentFrame >= 10 && currentFrame < 100) {
                output = QString("/Users/sharky/Desktop/screens/output0%1.png").arg(currentFrame);
            } else {
                output = QString("/Users/sharky/Desktop/screens/output%1.png").arg(currentFrame);
            }

            image.save(output);
        }

        currentFrame++;

        if (currentFrame > captureTimeStep) {
            recordSetting = SIMULATION;
            currentFrame = 0;
            vs->particleFrames.clear();
            playbackTimer.stop();
            simulationTimer.start(16, this);
        }
    } else if (event->timerId() == interpolationTimer.timerId()) {
        //Increment the cross-fade between Perlin noise maps
        if (frameTimer->elapsed() > 2000) {
            int elapsedTime = frameTimer->restart();
            printf("FPS: %f\n", float(frameCount) / float(elapsedTime/1000.0));
            frameCount = 0;
        }
        vs->incrementInterp();
    } else {
        QWidget::timerEvent(event);
    }
}

float CanvasWidget::findDensity() {
    float density = 0.0;

    float tilt1 = logisticsCurve(tiltMagnitude);
    float pressure1 = logisticsCurve(pressure);

    if (controlSetting == PRESSURE) {
        //Pressure-based density
        density = vs->max_density*pressure1;
    } else if (controlSetting == TILT) {
        //Tilt-based density
        density = vs->min_density + vs->max_density*tilt1;
    } else if (controlSetting == PLAIN) {
        //No influence density
        density = vs->min_density;
    } else {
        //Full influence density
        density = vs->min_density + vs->max_density*(0.5*tilt1+0.5*pressure1);
    }

    if (density > vs->max_density) {
        density = vs->max_density;
    } else if (density < vs->min_density) {
        density = vs->min_density;
    }
    return density;
}

double CanvasWidget::findRadius() {
    double radius = 0.0;

    float tilt1 = logisticsCurve(tiltMagnitude);
    float pressure1 = logisticsCurve(pressure);

    if (controlSetting == PRESSURE) {
        //Pressure-based width
        radius = vs->min_radius + vs->mouse_radius*pressure1;
    } else if (controlSetting == TILT) {
        //Tilt-based width
        radius = vs->min_radius + vs->mouse_radius*tilt1;
    } else if (controlSetting == VELOCITY) {
        //Velocity-based width
        double scale = 1.0 - mag(velocity)/30.0;
        radius = vs->min_radius + vs->mouse_radius*scale;
    } else if (controlSetting == PLAIN) {
        //No influence width
        radius = vs->mouse_radius;
    } else {
        //Full influence width
        double scale = 1.0 - mag(velocity)/30.0;
        radius = vs->min_radius + vs->mouse_radius*(0.3*tilt1+0.6*pressure1+0.1*scale);
    }

    if (radius < vs->min_radius) {
        return vs->min_radius;
    } if (radius > vs->max_radius) {
        return vs->max_radius;
    }
    return radius;
}

float CanvasWidget::findTurbulenceAmplifier() {
    float amplifier = 0.0;

    if (controlSetting == VELOCITY || controlSetting == ALL) {
        //Velocity influence speed
        amplifier = vs->fvf->turbulence_amplifier * logisticsCurve(mag(velocity));
    } else {
        //No influence speed
        amplifier = vs->fvf->turbulence_amplifier;
    }

    return amplifier;
}

float CanvasWidget::findDirectionalAmplifier() {
    float amplifier = 0.0;

    if (controlSetting == VELOCITY || controlSetting == ALL) {
        //Velocity influence speed
        amplifier = vs->bvf->directional_amplifier * logisticsCurve(mag(velocity)/20.0);
    } else {
        //No inflence speed
        amplifier = vs->bvf->directional_amplifier;
    }

    return amplifier;
}

void CanvasWidget::setShader() {
    switch (shaderSelection) {
    case ShaderSelection::SMOKE:
        currentShader = &smokeShader;
        break;
    case ShaderSelection::BUBBLE:
        currentShader = &bubbleShader;
        break;
    case ShaderSelection::FIRE:
        currentShader = &fireShader;
        break;
    case ShaderSelection::STAR:
        currentShader = &starShader;
        break;
    default:
        currentShader = &smokeShader;
        break;
    }
}

float CanvasWidget::logisticsCurve(float x) {
    return 1.0/(1.0 + 25*pow(2.718281, -7*x));
}


//Render particles both for simulation and playback
void CanvasWidget::renderParticles() {
    if (recordSetting == SIMULATION) {
        if (showTexture) {
            //Display particles using textures
            glEnable(GL_TEXTURE_2D);
            std::vector<Particle *>::iterator particle;
            for (particle = vs->particles.begin(); particle != vs->particles.end(); ++particle) {
                Particle *p = (*particle);
                p->shader->bind();

                if (p->shader->uniformLocation(QString("alpha")) != -1) {
                    p->shader->setUniformValue(p->shader->uniformLocation(QString("alpha")), p->alpha);
                }

                if (p->shader->uniformLocation(QString("color")) != -1) {
                    p->shader->setUniformValue(p->shader->uniformLocation(QString("color")),
                                               QVector4D(p->red, p->green, p->blue, 1.0));
                }

                texDraw(p->position, p->radius, p->w);
                p->shader->release();
            }
                glDisable(GL_TEXTURE_2D);
        } else {
            //Render as circles without texture
            std::vector<Particle *>::iterator particle;
            for (particle = vs->particles.begin(); particle != vs->particles.end(); ++particle) {
                Particle * p = (*particle);
                glColor4f(0.3, 0.3, 0.6, p->alpha);
                drawCircle(p->position[0], p->position[1], -0.5, 3, 6);
            }
        }
    } else {
        glUseProgramObjectARB(0);
        glEnable(GL_TEXTURE_2D);

        particlePlayback(currentFrame);

        glDisable(GL_TEXTURE_2D);
    }
}

void CanvasWidget::renderPoints() {
    std::vector<Touches *>::iterator touch;
    std::vector<Touches *>::iterator point;
    std::vector<Stroke *>::iterator stroke;

    //Render Points
    glUseProgramObjectARB(0);
    glPointSize(8);
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_POINTS);

    //Current Stroke
    for (point = vs->currentTouchStroke->points.begin(); point != vs->currentTouchStroke->points.end(); point++) {
        glVertex2fv((*point)->position.v);
    }

    //Other Strokes
    for (stroke = vs->strokes.begin(); stroke != vs->strokes.end(); stroke++) {
        for (point = (*stroke)->points.begin(); point != (*stroke)->points.end(); point++) {
            glVertex2fv((*point)->position.v);
        }
    }
    glEnd();

    //Render Touches
    glUseProgramObjectARB(0);
    glPointSize(3);
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_POINTS);

    //Current Stroke
    for (touch = vs->currentTouchStroke->touches.begin(); touch != vs->currentTouchStroke->touches.end(); touch++) {
        glVertex2fv((*touch)->position.v);
    }

    //Other Strokes
    for (stroke = vs->strokes.begin(); stroke != vs->strokes.end(); stroke++) {
        for (touch = (*stroke)->touches.begin(); touch != (*stroke)->touches.end(); touch++) {
            Touches * t = (*touch);
            glVertex2fv(t->position.v);
        }
    }
    glEnd();

    //Render Outer points
    glUseProgramObjectARB(0);
    glPointSize(2);
    glColor3f(0.0, 0.0, 1.0);
    glBegin(GL_POINTS);

    //Current Stroke
    for (touch = vs->currentTouchStroke->touches.begin(); touch != vs->currentTouchStroke->touches.end(); touch++) {
        glVertex2fv((*touch)->above.v);
        glVertex2fv((*touch)->below.v);
    }

    //Other Strokes
    for (stroke = vs->strokes.begin(); stroke != vs->strokes.end(); stroke++) {
        for (touch = (*stroke)->touches.begin(); touch != (*stroke)->touches.end(); touch++) {
            Touches * t = (*touch);
            glVertex2fv((*touch)->above.v);
            glVertex2fv((*touch)->below.v);
        }
    }
    glEnd();
}

//Playback particles for rendering to screen
void CanvasWidget::particlePlayback(int frame) {
    for (int i = 0; i < vs->particleFrames.size(); i++) {
        Particle * p = vs->particleFrames.at(i).at(frame);

        p->shader->bind();

        if (p->shader->uniformLocation(QString("alpha")) != -1) {
            p->shader->setUniformValue(p->shader->uniformLocation(QString("alpha")), p->alpha);
        }

        if (p->shader->uniformLocation(QString("color")) != -1) {
            p->shader->setUniformValue(p->shader->uniformLocation(QString("color")),
                                          QVector4D(p->red, p->green, p->blue, 1.0));
        }

        texDraw(p->position, p->radius, p->w);
        p->shader->release();
    }
}


//Draw the particles as smoke textures
void CanvasWidget::texDraw(Vec2f point, float radius, float angle) {
    float depth = -0.5;

    float untilted = M_PI/4;
    float xrotate1 = cosf(untilted+angle)*radius;
    float yrotate1 = sinf(untilted+angle)*radius;
    float xrotate2 = cosf(untilted-angle)*radius;
    float yrotate2 = sinf(untilted-angle)*radius;

    // Draw the texture
    glBegin(GL_QUADS);

    // Lower left corner
    glTexCoord2f(0.0, 1.0);
    glVertex3f(point[0]-xrotate1, point[1]-yrotate1, depth);

    // Lower right corner
    glTexCoord2f(1.0, 1.0);
    glVertex3f(point[0]+xrotate2, point[1]-yrotate2, depth);

    // Upper right corner
    glTexCoord2f(1.0, 0.0);
    glVertex3f(point[0]+xrotate1, point[1]+yrotate1, depth);

    // Upper left corner
    glTexCoord2f(0.0, 0.0);
    glVertex3f(point[0]-xrotate2, point[1]+yrotate2, depth);

    glEnd();
}

//Method modified from here: http://slabode.exofire.net/circle_draw.shtml
void CanvasWidget::drawCircle(float cx, float cy, float cz, float r, int num_segments) {
    float theta = 2 * 3.1415926 / float(num_segments);
    float tangetial_factor = tanf(theta);  //calculate the tangential factor

    float radial_factor = cosf(theta);   //calculate the radial factor

    float x = r;   //start at angle = 0
    float y = 0;

    glBegin(GL_TRIANGLE_FAN);
    glVertex3f(cx, cy, cz);
    for(int ii = 0; ii <= num_segments; ii++) {
        glVertex3f(x + cx, y + cy, cz);     //output vertex

        //calculate the tangential vector
        //remember, the radial vector is (x, y)
        //to get the tangential vector we flip those coordinates and negate one of them
        float tx = -y;
        float ty = x;

        //add the tangential vector
        x += tx * tangetial_factor;
        y += ty * tangetial_factor;

        //correct using the radial factor
        x *= radial_factor;
        y *= radial_factor;
    }
    glEnd();
}

//Render the heat map based on brush velocities
void CanvasWidget::drawBrushHeatMap() {
    for (int i = 0; i < vs->gridCellsX; i++) {
        for (int j = 0; j < vs->gridCellsY; j++) {

            Vec2f cell = vs->bvf->getCellVelocity(i*vs->gridCellsY+j);
            float red = abs(cell[0]);
            float green = abs(cell[1]);

            red /= 50.0;
            green /= 50.0;

            float x = i*vs->gridSizeX;
            float y = j*vs->gridSizeY;

            glColor3f(1.0-red, 1.0-green, 1.0);
            glBegin(GL_TRIANGLE_STRIP);
            glVertex3f(x, y+vs->gridSizeY, 0.0);
            glVertex3f(x, y, 0.0);
            glVertex3f(x+vs->gridSizeX, y+vs->gridSizeY, 0.0);
            glVertex3f(x+vs->gridSizeX, y, 0.0);
            glEnd();
        }
    }
}
