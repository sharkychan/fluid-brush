/*
 *  Touches.cpp
 *  CurlNoiseControls
 *
 *  Created by Sarah Abraham on 10/30/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "Touches.h"
#include <stdio.h>
#include "vec.h"

Touches::Touches(Vec2f _position, double _noise_gain, double _noise_length) {
    position = _position;
    above = _position;
    below = _position;
    index = 0;
    density = 0.0;
    noise_gain = _noise_gain;
    noise_length = _noise_length;
    radius = 50;

    velocity = Vec<2, float> (0.0, 0.0);
}

//Deep copy Touches t
Touches::Touches(Touches * _t) {
    position = _t->position;
    index = _t->index;
    above = _t->above;
    below = _t->below;
    noise_gain = _t->noise_gain;
    noise_length = _t->noise_length;
    radius = _t->radius;
    density = _t->density;
    turbulence_amplifier = _t->turbulence_amplifier;
    directional_amplifier = _t->directional_amplifier;
    velocity = _t->velocity;
    red = _t->red;
    green = _t->green;
    blue = _t->blue;
}

Touches::~Touches() {
}

//Set the touch's adjustable parameters
void Touches::setTouchParameters(float _density, float _radius, float _turbulence, float _directional) {
    density = _density;
    radius = _radius;
    turbulence_amplifier = _turbulence;
    directional_amplifier = _directional;
}

void Touches::setColor(float _red, float _green, float _blue) {
    red = _red;
    green = _green;
    blue = _blue;
}

void Touches::setIndex(int _index) {
    index = _index;
}

//Attach a directional velocity to the touch based on distance from previous touch
void Touches::setVelocity(Touches * prevTouch, int _culledPoints) {
    float vel_x = ((position[0] - prevTouch->position[0])/(float)_culledPoints);
    float vel_y = ((position[1] - prevTouch->position[1])/(float)_culledPoints);
    velocity = Vec<2, float> (vel_x, vel_y);
}


void Touches::printout() {
    printf("\n");
    printf("position = (%f, %f)\n", position[0], position[1]);

    printf("velocity = (%f, %f)\n", velocity[0], velocity[1]);

    printf("density = %f\n", density);
    printf("radius = %f\n", radius);
    printf("turbulence = %f\n", turbulence_amplifier);
    printf("directional = %f\n", directional_amplifier);
    printf("\n");
}

void Touches::generateOuterPoints(Vec2f endpoint) {
    float dx = position[0] - endpoint[0];
    float dy = position[1] - endpoint[1];
    float mag = sqrt(dx*dx + dy*dy);
    dx /= mag;
    dy /= mag;

    below = Vec<2, float> (position[0] + radius*dy, position[1] - radius*dx);
    above = Vec<2, float> (position[0] - radius*dy, position[1] + radius*dx);
}
