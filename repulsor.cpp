#include "repulsor.h"

Repulsor::Repulsor(Vec2f _position, double _radius) {
    position = _position;
    radius = _radius;

    c = 1;
    s = 0;
}

Repulsor::~Repulsor() {
    subdivisions.clear();
}

float Repulsor::distance(Vec2f p) {
    Vec2f q(c*(p[0]-position[0])-s*(p[1]-position[1]), s*(p[0]-position[0])+c*(p[1]-position[1]));
    float d=1e37;
    if (radius > 0) {
        float thisd = mag(q)-radius;
        if (thisd < d) {
            d = thisd;
        }
    }
    if (d < 0) {
        return 0;
    }
    return d;
}

