// minimal fragment shader
// www.lighthouse3d.com

#define between(v,x1,x2) (v>= x1 && v<=x2)
#define pi 3.141592653589793238462643383279

uniform float alpha;
uniform vec4 color;

void main() {
        //find the center of the texture for this fragment
	vec2 center = vec2(0.5,0.5);
        float dist = distance(center, gl_TexCoord[0].xy);

        float new_alpha = (1.0 - dist)*alpha;

        if (dist > 0.5) {
            new_alpha = 0.0;
  	}

        gl_FragColor = vec4(color.x, color.y, color.z, new_alpha);
}

