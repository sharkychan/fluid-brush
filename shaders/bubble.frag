float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

uniform float alpha;
uniform vec4 color;
varying vec2 v_texCoord2D;

void main()
{
	vec2 v = vec2(2.0 * v_texCoord2D.xy);  

	//find the center of the texture for this fragment
	vec2 center = vec2(0.5,0.5);
    	float dist = distance(center, v_texCoord2D.xy);
    	
        float alpha_final = alpha*2.0;

        if (dist > 0.5) {
            alpha_final = 0.0;
        }

	vec4 newcolor = vec4(1.0, 1.0, 1.0, alpha_final)*color;

        vec2 reflection = vec2(0.7, 0.3);
        if (distance(reflection, v_texCoord2D.xy) < 0.1) {
		newcolor.x = 1.0;
		newcolor.y = 1.0;
		newcolor.z = 1.0;
	}

	gl_FragColor = newcolor;
}
