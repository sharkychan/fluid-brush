uniform float alpha;
uniform vec4 color;
varying vec2 v_texCoord2D;

void main()
{
	vec2 v = vec2(2.0 * v_texCoord2D.xy);  

	//find the center of the texture for this fragment
	vec2 center = vec2(0.5,0.5);
    	float dist = distance(center, v_texCoord2D.xy);
    	
     float alpha_final = alpha*2.0;
	vec4 new_color = color;

	float top_dist = distance(vec2(1.0, 0.0), v_texCoord2D.xy);
	float top_area = 0.46;
	if (top_dist <= top_area) {
		alpha_final = 0.0;
	}

	top_dist = distance(vec2(0.0, 0.0), v_texCoord2D.xy);
	if (top_dist <= top_area) {
		alpha_final = 0.0;
	}

	top_dist = distance(vec2(0.0, 1.0), v_texCoord2D.xy);
	if (top_dist <= top_area) {
		alpha_final = 0.0;
	}

	top_dist = distance(vec2(1.0, 1.0), v_texCoord2D.xy);
	if (top_dist <= top_area) {
		alpha_final = 0.0;
	}



	vec4 newcolor = vec4(1.0, 1.0, 1.0, alpha_final)*new_color;

	gl_FragColor = newcolor;
}

