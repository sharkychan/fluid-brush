#include "velocitysystem.h"

VelocitySystem::VelocitySystem(int width, int height) {
    window_width = width;
    window_height = height;

    gridCellsX = 64;
    gridCellsY = 64;
    gridSizeX = ceilf((float)width/gridCellsX);
    gridSizeY = ceilf((float)height/gridCellsY);

    fvf = new FluidVectorField(width, height);
    bvf = new BrushVectorField(gridCellsX, gridCellsY, gridSizeX, gridSizeY);

    //Tweakable parameters
    mouse_radius = 20;
    max_radius = 80;

    max_density = 0.15;
    min_density = 0.05;

    initial_alpha = 0.1;

    //values that don't currently change
    noise_gain = 0.3;
    noise_lengthscale = 50.0;

    simulation_timestep = 0.04;

    //Start program timer
    programTime = new QTime();
    programTime->start();
}

VelocitySystem::~VelocitySystem() {
    delete fvf;
    delete bvf;
    delete currentTouchStroke;
    delete programTime;

    strokes.clear();
    particleFrames.clear();
}

void VelocitySystem::printState() {
    printf("Total Strokes = %zu\n", strokes.size());

    std::vector<Stroke *>::iterator stroke;
    for (stroke = strokes.begin(); stroke != strokes.end(); stroke++) {
        Stroke * s = (*stroke);
        printf("Touches in Stroke = %zu\n", s->touches.size());
    }

    printf("Total Particles = %zu\n", particles.size());
}

//Generate particles and associate with parent stroke and touch
void VelocitySystem::generateParticles(Stroke &s, Touches &t0, Touches &t1, float initial_alpha, QGLShaderProgram&  _shader) {
    Vec2f v = t1.position - t0.position;
    Vec2f perpendicular = perp(v);
    normalize(perpendicular);
    Vec2f perp1 = t0.above - t0.below;
    normalize(perp1);
    Vec2f perp2 = t1.above - t1.below;
    normalize(perp2);

    //take area of trapezoid
    float upperLength = dist(t0.above, t1.above);
    float lowerLength = dist(t0.below, t1.below);
    float area = (upperLength + lowerLength)* 0.5 * t0.radius;
    float n = t0.density * area;

    for (int k = 0; k < (float)n; k++) {
        float d = float(rand())/float(RAND_MAX);    //Between 0.0 and 1.0
        Vec2f initial_pos = t0.position + d * (t1.position - t0.position);   //Position along the line segment
        float radius = t0.radius*d + t1.radius*(1 - d);
        float distance = ((float(rand())/float(RAND_MAX) * 2 * radius) - radius);

        Vec2f new_pos = initial_pos;
        if (d < 0.5 && !std::isnan(perp1[0])) {
            //Particle is positioned along first half of segment, so use perp1
            new_pos += perp1*distance;
        } else if (d >= 0.5 && !std::isnan(perp2[0])) {
            //else use perp2
            new_pos += perp2*distance;
        } else {
            //Or default to regular perpendicular to avoid nans :|
            new_pos += perpendicular*distance;
        }

        Particle * p = new Particle(&s, &t0, new_pos, distance, initial_alpha);
        p->red = t0.red;
        p->green = t0.green;
        p->blue = t0.blue;
        p->shader = &_shader;

        particles.push_back(p);
    }
}

void VelocitySystem::generateParticles(Stroke &s, Touches &t, float initial_alpha, QGLShaderProgram& _shader) {
    float n = t.density*10 * t.radius;
    for (int k = 0; k < (float)n; k++) {
        float distance = (t.radius*rand()) / (RAND_MAX + 1.0);
        float angle = (2*M_PI*rand()) / (RAND_MAX + 1.0);
        Vec2f position = Vec<2, float> (t.position[0] + distance*cosf(angle),
                t.position[1] + distance*sinf(angle));

        Particle * p = new Particle(&s, &t, position, distance, initial_alpha);
        p->red = t.red;
        p->green = t.green;
        p->blue = t.blue;
        p->shader = &_shader;

        particles.push_back(p);
    }
}

//Update particle positions along their parent strokes
void VelocitySystem::moveParticles() {
    //Currently Eulerian because that seems sufficient

    std::vector<Particle *>::iterator particle;
    for (particle = particles.begin(); particle != particles.end(); ++particle) {
        Particle &p = *(*particle);
        Stroke &s = *p.parent_stroke;
        Touches &t = *p.parent_touch;

        if (s.touches.size() == 1) {
            //If there's only one touch, handle using a radius
            if (dist(t.position, p.position) > t.radius) {
                repositionParticle(t, t, p);
            }
        } else {
            //If there are multiple touches, consider the particle's parent stroke as a line segment
            if (&t == s.touches.at(0) || &t == s.touches.at(s.touches.size()-1)) {
                //Particle's parent is last touch in stroke
                if (dist(p.position, t.position) > t.radius*1.5) {
                    reparentParticle(s, p);
                }
            } else {
                //Particle is somewhere along the middle of the stroke
                handleMidParticles(s, t.index, p);
            }
        }

        //Update alpha then move particle along the stroke
        float alphaRamp = p.distance/t.radius;
        //Alpha decreases as distance from touch increases
        p.alpha = clamp(initial_alpha*clamp(1-alphaRamp));

        float amplifier = p.parent_touch->turbulence_amplifier;
        int nextIndex = p.parent_touch->index+1;
        if (nextIndex < s.touches.size()) {
            //If there is a neighboring touch, interpolate turbulence amplification accordingly
            Touches * t = p.parent_touch;
            Touches * nextTouch = s.touches.at(nextIndex);
            float d = clamp(dist(t->position, p.perpendicular_position)/dist(t->position, nextTouch->position));
            amplifier = t->turbulence_amplifier + d * (nextTouch->turbulence_amplifier - t->turbulence_amplifier);
        }
        updateParticle(p, amplifier);
    }
}

void VelocitySystem::updateParticle(Particle &p, float amplifier) {
    Vec2f fv = fvf->get_velocity(p.position, p.parent_touch, amplifier);
    Vec2f bv = bvf->get_velocity(p.position);

    Vec2f v = bv + fv;

    //Assign the particle's linear and angular velocities
    p.w = (mag(v)/p.distance);     //In radians
    p.v = v;
    p.position += simulation_timestep*v;

}

//Remove all items on canvas
void VelocitySystem::resetParticles() {
    strokes.clear();
    currentTouchStroke->touches.clear();
    currentTouchStroke->points.clear();
    particles.clear();
}

//Erase touches within a given radius
void VelocitySystem::eraseTouches(int x, int y) {
    //Look through all strokes
    Vec2f position = Vec<2, float> (x, y);
    for (size_t i = 0; i < strokes.size(); i++) {
        Stroke * s = strokes.at(i);
        //s->eraseTouches(x, y, mouse_radius);
        std::vector<Touches *>::reverse_iterator touch;
        for (touch = s->touches.rbegin(); touch != s->touches.rend(); ++touch) {
            //Look through all touches within a stroke
            Touches * t = (*touch);
            float distance = dist(t->position, position);
            if (distance <= mouse_radius) {
                //Remove touch from stroke
                s->touches.erase(--touch.base());
            }
        }
    }
}

//Erase all particles within a given radius
void VelocitySystem::eraseParticles(int x, int y) {
    Vec2f position = Vec<2, float> (x, y);
    std::vector<Particle *>::reverse_iterator particle;
    for (particle = particles.rbegin(); particle != particles.rend(); ++particle) {
        //If it's within given distance to (x, y) pos, remove it
        Particle * p = (*particle);
        float distance = dist(p->position, position);
        if (distance <= mouse_radius) {
            particles.erase(--particle.base());
        }
    }
}

float VelocitySystem::clamp(float value) {
    if (value > 1.0) {
        return 1.0;
    } else if (value < 0.0) {
        return 0.0;
    } else {
        return value;
    }
}

//Create a vector for each particle currently on the screen to be stored for frame capture
void VelocitySystem::createParticleStates() {
    std::vector<Particle *>::iterator particle;
    for (particle = particles.begin(); particle != particles.end(); ++particle) {
        std::vector<Particle *> particleState;
        particleFrames.push_back(particleState);
    }
}

//Store all particles in a vector and push back into particleFrames vector of vectors
void VelocitySystem::storeCurrentState() {
    std::vector<Particle *>::iterator particle;
    int index = 0;
    for (particle = particles.begin(); particle != particles.end(); ++particle) {
        particleFrames.at(index).push_back(new Particle(*particle));
        index++;
    }
}

//Adjust an individual particle's looping within the larger frame loop
void VelocitySystem::adjustCurrentState() {
    //Go through each particle's vector of positions within the particleFrames vector
    for (int i = 0; i < particleFrames.size(); i++) {
        int index = rand() % particleFrames.at(i).size();

        //Array rotation by index
        std::reverse(particleFrames.at(i).begin(), particleFrames.at(i).end());
        std::reverse(particleFrames.at(i).begin(), particleFrames.at(i).begin()+index);
        std::reverse(particleFrames.at(i).begin()+index, particleFrames.at(i).end());
    }
}

void VelocitySystem::incrementInterp() {
    fvf->incrementInterp();
}

//Return the particle's current parent touch and update its distance from that particle
Touches * VelocitySystem::handleMidParticles(const Stroke &s, int touch_index, Particle &p) {
    Touches * t0 = s.touches.at(touch_index);
    Touches * t1 = s.touches.at(touch_index+1);
    Touches * newParent;
    std::vector<Touches *>::const_iterator touch = s.touches.begin();
    std::vector<Touches *>::const_reverse_iterator rtouch = s.touches.rbegin();

    float distance = distanceFromLineSegment(t0->position, t1->position, p);

    if (distance >= 0) {
        //Particle is within current touch's range
        //Lerp between t0 and t1 radius to find if particle is within the quadrant
        float d = dist(t0->position, p.perpendicular_position)/dist(t0->position, t1->position);
        float radius = t0->radius + d * (t1->radius - t0->radius);

        if (distance > radius) {
            //Reposition p within t's radius
            repositionParticle(*t0, *t1, p);
        }
        return t0;
    } else if (dist(p.position, t0->position) >= dist(p.position, t1->position)) {
        //Go forward along touch
        if (touch_index < s.touches.size()-1) {
            advance(touch, touch_index);
            newParent = moveForwardAlongTouches(s, touch, p);
        } else {
            //Attach to a random parent
            newParent = reparentParticle(s, p);
        }
        return newParent;
    } else {
        //Go backward along touch
        if (touch_index > 0) {
            advance(rtouch, touch_index);
            newParent = moveBackwardAlongTouches(s, rtouch, p);
        } else {
            //Attach to a random parent
            newParent = reparentParticle(s, p);
        }

        int index = 0;
        for (int i = 0; i < s.touches.size()-1; i++) {
            if (newParent == s.touches.at(i)) {
                index = i;
            }
        }

        Touches * nextTouch;
        if (index+1 < s.touches.size()-1) {
            nextTouch = s.touches.at(index+1);
        } else {
            nextTouch = newParent;
        }

        distance = distanceFromLineSegment(newParent->position, nextTouch->position, p);
        float d = dist(newParent->position, p.perpendicular_position)/dist(newParent->position, nextTouch->position);
        float radius = newParent->radius + d * (nextTouch->radius - newParent->radius);
        if (distance > radius) {
            repositionParticle(*newParent, *nextTouch, p);
        } else if (distance < 0) {
            float dist1 = dist(newParent->position, p.position);
            float dist2 = dist(nextTouch->position, p.position);
            if (dist1 > newParent->radius && dist2 > nextTouch->radius) {
                repositionParticle(*newParent, *nextTouch, p);
            }
        }

        return newParent;
    }
}

//Reposition a particle within touch t's quadrant
float VelocitySystem::repositionParticle(const Touches &t0, const Touches &t1, Particle &p) {
    float distance;
    if (&t0 != &t1) {
        Vec2f v = t1.position - t0.position;
        Vec2f perpendicular = perp(v);
        normalize(perpendicular);

        float d = float(rand())/float(RAND_MAX); //Between 0.0 and 1.0
        Vec2f initial_pos = t1.position*d + (1 - d)*t0.position;
        distance = ((float(rand())/float(RAND_MAX) * t1.radius) - (t1.radius/2.0));
        Vec2f new_pos = initial_pos + perpendicular*distance;

        p.position = new_pos;
        p.distance = distance;
        p.perpendicular_position = initial_pos;
    } else {
        distance = (t0.radius*rand()) / (RAND_MAX + 1.0);
        float angle = (2*M_PI*rand()) / (RAND_MAX + 1.0);

        p.position[0] = t0.position[0] + distance*cosf(angle);
        p.position[1] = t0.position[1] + distance*sinf(angle);
        p.distance = distance;
        p.perpendicular_position = t0.position;
    }

    return distance;
}

//Reparent particle to a random touch within the stroke
Touches * VelocitySystem::reparentParticle(const Stroke &s, Particle &p) {
    int index = rand() % s.touches.size();
    std::vector<Touches *>::const_iterator touch = s.touches.begin();
    std::advance(touch, index);
    Touches * t0 = (Touches *) (*touch);
    if (s.touches.size() > 1 && index < s.touches.size()-1) {
       ++touch;
    }
    Touches * t1 = (Touches *) (*touch);
    repositionParticle(*t0, *t1, p);
    p.parent_touch = t0;

    return t0;
}

//Move forward along the stroke to find the new parent touch
Touches * VelocitySystem::moveForwardAlongTouches(const Stroke &s, std::vector<Touches *>::const_iterator touch, Particle &p) {
    Touches * t0 = (Touches *) (* touch);
    ++touch;
    for (; touch != s.touches.end(); ++touch) {
        Touches * t1 = (Touches *) (*touch);
        float distance = distanceFromLineSegment(t0->position, t1->position, p);
        if (distance >= 0 /*|| distance == -2.0*/) {
            return t0;
        }
        t0 = t1;
    }

    //If no inner touch is found, attach to a random point
    return reparentParticle(s, p);
}

//Move backward along the stroke to find the new parent touch
Touches * VelocitySystem::moveBackwardAlongTouches(const Stroke &s, std::vector<Touches *>::const_reverse_iterator touch, Particle &p) {
    Touches * t0 = (Touches *) (* touch);
    ++touch;
    for (; touch != s.touches.rend(); ++touch) {
        Touches * t1 = (Touches *) (*touch);
        float distance = distanceFromLineSegment(t0->position, t1->position, p);
        if (distance >= 0 /*|| distance == -1.0*/) {
            return t0;
        }
        t0 = t1;
    }

    //If no inner touch is found, attach to a random point
    return reparentParticle(s, p);
}

//Check distance from particle position to line segment
float VelocitySystem::distanceFromLineSegment(const Vec2f &t0, const Vec2f &t1, Particle &p) {
    Vec2f v = t1 - t0;
    Vec2f w = p.position - t0;

    float c1 = dot(w, v);
    float c2 = dot(v, v);

    if (c1 <= 0) {
        //If c1 <= 0, point is "in front of" t0, so return -2.0
        p.perpendicular_position = t0;
        p.distance = dist(p.position, t0);
        return -2.0;
    } else if (c2 <= c1) {
        //If c2 <= c1 point is "behind" t1, so return -1.0
        p.perpendicular_position = t1;
        p.distance = dist(t1, p.position);
        return -1.0;
    }

    //Within the current line segment
    float b = c1/c2;
    Vec2f pb = t0 + b*v;
    float distance = dist(p.position, pb);

    //Store this point on the line segment to the particle
    p.perpendicular_position = pb;
    p.distance = distance;

    //Return particle's distance from this point
    return distance;
}
