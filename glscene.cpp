#include "glscene.h"

#include <QtOpenGL>


GLScene::GLScene(int width, int height) {
    fs = new FluidSystem(width, height);
    showTexture = 0;
    showPoints = 1;

    //Start timer for screen refresh
    refreshTimer = new QTimer(this);
    connect(refreshTimer, SIGNAL(timeout()), this, SLOT(refresh()));

    //Start timer for simulation step
    simulationTimer = new QTimer(this);
    connect(simulationTimer, SIGNAL(timeout()), this, SLOT(simStep()));
    simulationTimer->start(100);

    //Start time for the texture interpolation
    interpolationTimer = new QTimer(this);
    connect(interpolationTimer, SIGNAL(timeout()), this, SLOT(interpStep()));
    interpolationTimer->start(2000);
}

//TODO: Destructor method here
GLScene::~GLScene() {
    delete fs;
}

void GLScene::connectToCanvas(CanvasWidget *_cw) {
    cw = _cw;
}

void GLScene::drawBackground(QPainter *painter, const QRectF &) {

    if (painter->paintEngine()->type() != QPaintEngine::OpenGL) {
        qWarning("OpenGLScene: drawBackground needs a QGLWidget to be set as viewport on the graphics view");
        return;
    }
}

void GLScene::drawForeground(QPainter *painter, const QRectF &) {
    if (painter->paintEngine()->type() != QPaintEngine::OpenGL) {
        qWarning("OpenGLScene: drawBackground needs a QGLWidget to be set as viewport on the graphics view");
        return;
    }

    //Display particles as textures
    if (showTexture) {
        glUseProgramObjectARB(0);
        glEnable(GL_TEXTURE_2D);

        //TODO: Go through step sizes of particles and draw all textures
        for (int i = 0; i < fs->particles.size(); i++) {
            if (i%2 == 0) {
                glBindTexture(GL_TEXTURE_2D, cw->smokeTextures.at(0));
            } else {
                glBindTexture(GL_TEXTURE_2D, cw->smokeTextures.at(1));
            }
            drawTexture(fs->particles[i]->position, fs->particles[i]->radius, fs->particles[i]->alpha);
        }

        glDisable(GL_TEXTURE_2D);

    } else {
        //Display particles with shaders
        //glUseProgramObjectARB(cloud_shader);

        glColor3f(1.0, 0.0, 0.0); //Use until shader is hooked up
        for (int i = 0; i < fs->particles.size(); i++) {
            drawCircle(fs->particles[i]->position[0], fs->particles[i]->position[1], (float)rand()/(float)RAND_MAX, 3, 6);
        }
    }

    //Show points
    if (showPoints) {
        glUseProgramObjectARB(0);
        glPointSize(4);
        glColor3f(1.0, 0.0, 0.0);
        glBegin(GL_POINTS);
        for (int i = 0; i < fs->touches.size(); i++) {
            glVertex2fv(fs->touches[i]->position.v);
        }

        for (int i = 0; i < fs->strokes.size(); i++) {
            for (int j = 0; j < fs->strokes[i]->touches.size(); j++) {
                Touches * touch = fs->strokes[i]->touches.at(j);
                glVertex2fv(touch->position.v);
            }
        }

        glEnd();

    }
}

//Called when user presses the mouse button
void GLScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    QPointF position = event->lastScenePos();
    int x = position.x();
    int y = position.y();

    if (event->button() == Qt::LeftButton) {
        printf("Left Mouse Pressed: %d, %d\n", x, y);
        if (activeButton != RIGHT) {
            activeButton = LEFT;
            fs->createTouches(x, y);
        }
    } else if (event->button() == Qt::RightButton)  {
        printf("Right Mouse Pressed: %d, %d\n",x, y);
        activeButton = RIGHT;
        fs->eraseTouches(x, y);
    }

    update();
}

//Called when user releases the mouse button
void GLScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        activeButton = NONE;

        //Add touches to current Stroke
        fs->currentStroke->addTouchVector(fs->touches);

        //Add current Stroke to vector of Strokes
        fs->strokes.push_back(fs->currentStroke);

        //clear touches vector
        fs->touches.clear();

    } else if (event->button() == Qt::RightButton) {
        activeButton = NONE;
    }

    update();
}

//Called when user presses a button then drags the mouse
void GLScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    QPointF position = event->lastScenePos();
    int x = position.x();
    int y = position.y();

    if (activeButton == LEFT) {
        fs->createTouches(x, y);
    } else if (activeButton == RIGHT) {
        fs->eraseTouches(x, y);
    }

    update();
}

void GLScene::wheelEvent(QGraphicsSceneWheelEvent * wheelEvent) {

}

//Begin calls to refresh the screen
void GLScene::startRefresh() {
    refreshTimer->start(100);
}

//Call on screen refresh
void GLScene::refresh() {
    update();
}

//Call on simulation step
void GLScene::simStep() {
    fs->moveParticles();
}

//Call on a texture interpolation step
void GLScene::interpStep() {
    fs->incrementInterp();
}

//Initialize stuff for OpenGL
void GLScene::initGL() {
    window_width = width();
    window_height = height();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, window_width, window_height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, window_width, 0, window_height);

    glClearColor(1.0, 1.0, 1.0, 1.0);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void GLScene::drawTexture(Vec2f point, float radius, float alpha) {
    float depth = (float)rand()/(float)RAND_MAX;

    glColor4f(0.7, 0.6, 0.7, alpha);

    // Draw the texture
    glBegin(GL_QUADS);

    // Lower left corner
    glTexCoord2f(0.0, 1.0);
    glVertex3f(point[0]-radius, point[1]-radius, depth);

    // Lower right corner
    glTexCoord2f(1.0, 1.0);
    glVertex3f(point[0]+radius, point[1]-radius, depth);
    // Upper right corner
    glTexCoord2f(1.0, 0.0);
    glVertex3f(point[0]+radius, point[1]+radius, depth);

    // Upper left corner
    glTexCoord2f(0.0, 0.0);
    glVertex3f(point[0]-radius, point[1]+radius, depth);

    glEnd();
}

//Method modified from here: http://slabode.exofire.net/circle_draw.shtml
void GLScene::drawCircle(float cx, float cy, float cz, float r, int num_segments) {
    float theta = 2 * 3.1415926 / float(num_segments);
    float tangetial_factor = tanf(theta);  //calculate the tangential factor

    float radial_factor = cosf(theta);   //calculate the radial factor

    float x = r;   //start at angle = 0

    float y = 0;

    glBegin(GL_TRIANGLE_FAN);
    glVertex3f(cx, cy, cz);
    for(int ii = 0; ii < num_segments; ii++) {
        glVertex3f(x + cx, y + cy, cz);//output vertex

        //calculate the tangential vector
        //remember, the radial vector is (x, y)
        //to get the tangential vector we flip those coordinates and negate one of them
        float tx = -y;
        float ty = x;

        //add the tangential vector
        x += tx * tangetial_factor;
        y += ty * tangetial_factor;

        //correct using the radial factor
        x *= radial_factor;
        y *= radial_factor;
    }
    glEnd();
}
