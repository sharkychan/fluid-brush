#ifndef BRUSHVECTORFIELD_H
#define BRUSHVECTORFIELD_H

#import "vec.h"
#import "Touches.h"
#import "Stroke.h"
#include <vector>

class BrushVectorField {
public:
    struct BrushCell {
        Vec2f velocity;
        std::vector<int> stroke_ids;
        int temp_stroke_id;
        int index;
    };

    BrushVectorField(int _rows, int _columns, int _cell_width, int _cell_height);
    ~BrushVectorField();

    void initializeVectorField();
    void updateVectorField(const Stroke &s);
    void addTouchVelocities(const Stroke &s);
    void addStrokeVelocities(const Stroke &s);
    void diffuseStrokeVelocities(const Stroke &s);

    Vec2f get_velocity(const Vec2f &position);
    Vec2f getCellVelocity(int index);
    Vec2f getCellVelocity(const Vec2f &position);
    int getStrokeId(int index);

    void removeStroke(const Stroke& s);

    void printState();

    void doCosineInterp(const Touches &t0, const Touches &t1, int id);
    void doCubicInterp(const Touches &t0, const Touches &t1, const Touches &t2, const Touches &t3, int id);

    //Helper methods
    int calculateIndex(const Vec2f &position);
    Vec2f lerp(const Vec2f &v0, const Vec2f &v1, float t);
    Vec2f cosineInterpolation(const Vec2f &p0, const Vec2f &p1, float mu);
    Vec2f cubicInterpolation(const Vec2f &p0, const Vec2f &p1, const Vec2f &p2, const Vec2f &p3, float mu);
    bool containsStrokeId(std::vector<int> cell_strokes, int _id);

    int rows;
    int columns;
    int cell_width;
    int cell_height;

    float directional_amplifier;

    BrushCell * vectorField;
    std::vector<BrushCell>updatedCells;
};

#endif // BRUSHVECTORFIELD_H
