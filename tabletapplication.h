#ifndef TABLETAPPLICATION_H
#define TABLETAPPLICATION_H

#include <QApplication>

class TabletApplication : public QApplication
{
    Q_OBJECT

public:
    TabletApplication(int &argv, char **args)
     : QApplication(argv, args) {}

     bool event(QEvent *event);
};

#endif // TABLETAPPLICATION_H
