#ifndef CONNECTORVARIABLE_H
#define CONNECTORVARIABLE_H

#include <QtCore/QObject>

class ConnectorVariable : public QObject {
     Q_OBJECT
public:
    ConnectorVariable(QObject *parent = 0);
    int value();

public slots:
    void setValue(int value);

signals:
    void valueChanged(double newValue);

private:
    int m_value;
};

#endif // CONNECTORVARIABLE_H
